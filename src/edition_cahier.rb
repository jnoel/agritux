# coding: utf-8

class EditionCahier < Prawn::Document
	
	def initialize annee, fichier, window
	
		super(	:page_layout => :portrait,
						:page_size => 'A4'
				 )
		font "./resources/fonts/Cantarell-Regular.ttf"
		
		societe = Societe.first
		
		annee = DateTime.now.year if annee.to_s.empty?
		
		en_tete annee, societe
		intrant annee
		
		parcelles = Parcelle.order(:id)
		parcelles.each do |p|
			en_tete_parcelle p
			engrais p.id, annee
			phyto p.id, annee
			recolte p.id, annee
		end
		
		pieds annee, societe
		
		rendu fichier
		
	end
	
	def en_tete annee, societe
		
		x = 50
		y = bounds.top-200
		size = 22
		
		fill_color "dedede"
		rectangle [x, y], 400, 200
		fill		

		fill_color "000000"
		bounding_box([x+20,y-50], :width => 360) do
			text "#{societe.nom}", :size => size, :styles => [:bold], :align => :center
			text "CAHIER DE CULTURE", :size => size, :styles => [:bold], :align => :center
			text "Année #{annee}", :size => size, :styles => [:bold], :align => :center
		end
	
	end
	
	def en_tete_parcelle parcelle
	
		start_new_page
		
		x = bounds.left
		y = bounds.top
		w = bounds.width
		h = 20
		
		fill_color "dedede"
		rectangle [x, y], w, h
		fill		

		fill_color "000000"		
		bounding_box([x+10,y-5], :width => w-x*2) do
			text "#{parcelle.designation} (taille = #{parcelle.taille} m²)", :size => 14, :styles => [:bold]
		end
		
		move_down 30
	
	end
	
	def engrais parcelle_id, annee
		
		text "Engrais", :size => 14, :styles => [:bold]
		
		datedeb = Date.parse("01/01/#{annee}")
		datefin = Date.parse("31/12/#{annee}")
		
		engrais = Cultureligne.includes(:article).where(:date => datedeb..datefin, :parcelle_id => parcelle_id, "articles.articlegroupe_id" => 1).order(:date)
		
		res_array = []
		
		engrais.each do |e|
			l = []
			l << e.date.strftime("%d/%m/%Y")
			l << e.article_designation
			l << "#{e.quantite} #{e.unite}"
			res_array << l
		end

		data = [[]]
		data[0] << "Date d'application"
		data[0] << "Type"
		data[0] << "Quantité"
		data += res_array
		 		
		bounding_box([0,cursor], :width => 500) do
  		t = table(data) do |table|
  			table.header = true
  			table.row(0).style :borders => [:top, :bottom, :left, :right]
  			table.row(0).style :align => :center
  			table.row(1..data.count).style :borders => [:left, :right]
  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
  			table.cell_style = { :size => 12, :border_color => "aaaaaa"}
  			table.column(2..5).align = :right
  			table.row(0).style :align => :center
  			table.column(0).align = :left
			end
		end
		
		move_down 30
	
	end
	
	def phyto parcelle_id, annee
		
		text "Produits phytopharmaceutiques", :size => 14, :styles => [:bold]
		
		datedeb = Date.parse("01/01/#{annee}")
		datefin = Date.parse("31/12/#{annee}")
		
		engrais = Cultureligne.includes(:article).where(:date => datedeb..datefin, :parcelle_id => parcelle_id, "articles.articlegroupe_id" => 4).order(:date)
		
		res_array = []
		
		engrais.each do |e|
			l = []
			l << e.date.strftime("%d/%m/%Y")
			l << e.article_designation
			l << "#{e.quantite} #{e.unite}"
			l << e.raison
			l << e.methode
			res_array << l
		end

		data = [[]]
		data[0] << "Date d'application"
		data[0] << "Type"
		data[0] << "Quantité"
		data[0] << "Raison du traitement"
		data[0] << "Méthode de traitement"
		data += res_array
		 		
		bounding_box([0,cursor], :width => 500) do
  		t = table(data) do |table|
  			table.header = true
  			table.row(0).style :borders => [:top, :bottom, :left, :right]
  			table.row(0).style :align => :center
  			table.row(1..data.count).style :borders => [:left, :right]
  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
  			table.cell_style = { :size => 12, :border_color => "aaaaaa"}
  			table.column(2..5).align = :right
  			table.row(0).style :align => :center
  			table.column(0).align = :left
  			#table.column(0).row(1..data.count).size = 30
			end
		end
		
		move_down 30
	
	end
	
	def intrant annee
		
		start_new_page
		
		text "Intrants agricoles pour l'exploitation", :size => 14, :styles => [:bold]
		
		datedeb = Date.parse("01/01/#{annee}")
		datefin = Date.parse("31/12/#{annee}")
		
		engrais = Articleentree.includes(:article).where(:date => datedeb..datefin, "articles.articlegroupe_id" => (1..6).to_a).order(:date)
		
		res_array = []
		
		engrais.each do |e|
			l = []
			l << e.date.strftime("%d/%m/%Y")
			l << e.article.designation
			l << "#{e.quantite} #{e.unite}"
			res_array << l
		end

		data = [[]]
		data[0] << "Date d'application"
		data[0] << "Type"
		data[0] << "Quantité"
		data += res_array
		 		
		bounding_box([0,cursor], :width => 500) do
  		t = table(data) do |table|
  			table.header = true
  			table.row(0).style :borders => [:top, :bottom, :left, :right]
  			table.row(0).style :align => :center
  			table.row(1..data.count).style :borders => [:left, :right]
  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
  			table.cell_style = { :size => 12, :border_color => "aaaaaa"}
  			table.column(2..5).align = :right
  			table.row(0).style :align => :center
  			table.column(0).align = :left
  			#table.column(0).row(1..data.count).size = 30
			end
		end
		
		move_down 30
	
	end
	
	def recolte parcelle_id, annee
		
		text "Récoltes", :size => 14, :styles => [:bold]
		
		datedeb = Date.parse("01/01/#{annee}")
		datefin = Date.parse("31/12/#{annee}")
		
		engrais = Cultureligne.includes(:culture).where(:date => datedeb..datefin, :parcelle_id => parcelle_id, :article_designation => "Vente").order(:date)
		
		res_array = []
		
		engrais.each do |e|
			l = []
			l << e.date.strftime("%d/%m/%Y")
			l << e.culture.designation
			l << "#{e.quantite} #{e.unite}"
			res_array << l
		end

		data = [[]]
		data[0] << "Date d'application"
		data[0] << "Type"
		data[0] << "Quantité"
		data += res_array
		 		
		bounding_box([0,cursor], :width => 500) do
  		t = table(data) do |table|
  			table.header = true
  			table.row(0).style :borders => [:top, :bottom, :left, :right]
  			table.row(0).style :align => :center
  			table.row(1..data.count).style :borders => [:left, :right]
  			table.row(data.count-1).style :borders => [:left, :right, :bottom]
  			table.cell_style = { :size => 12, :border_color => "aaaaaa"}
  			table.column(2..5).align = :right
  			table.row(0).style :align => :center
  			table.column(0).align = :left
  			#table.column(0).row(1..data.count).size = 30
			end
		end
		
		move_down 30
	
	end
	
	def pieds
		
		draw_text "Nombre de casiers : #{@casiers.count}", :at => [0,cursor-25], :size => 12, :styles => [:bold]
		
	end
	
	def pieds annee, societe
	
		number_pages "#{societe.nom} - Cahier de culture - Année #{annee}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		number_pages "<page>/<total>", {:start_count_at => 1, :at => [bounds.right - 100, 0], :align => :right, :size => 8}
	
	end
	
	def rendu fichier

		render_file fichier
	
	end
	
end
