# coding: utf-8

class DialCalendar < Gtk::Dialog

	attr_reader :cal
	
	def initialize window
		
		super("Calendrier", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 500, 500
		
		@cal = Gtk::Calendar.new
		
		@cal.signal_connect("day-selected-double-click") {
			self.response(Gtk::Dialog::RESPONSE_OK)
		}
		
		self.vbox.pack_start @cal, true, true, 3
		
		self.vbox.show_all

	end

end
