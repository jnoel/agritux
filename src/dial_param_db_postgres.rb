# coding: utf-8

class DialParamDb < Gtk::Dialog

	attr_reader :nom, :host, :database, :port, :username, :password, :chemin_documents
	
	def initialize window, id
		
		super("Paramètres de la base de données PostgreSQL", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 500, 500
		
		@nom = Gtk::Entry.new
		@nom.text = window.config_db.bases[id]["nom"].to_s if id>-1
		@host = Gtk::Entry.new
		@host.text = window.config_db.bases[id]["host"].to_s if id>-1
		@database = Gtk::Entry.new
		@database.text = window.config_db.bases[id]["database"].to_s if id>-1
		@port = Gtk::Entry.new
		@port.text = window.config_db.bases[id]["port"].to_s if id>-1
		@username = Gtk::Entry.new
		@username.text = window.config_db.bases[id]["username"].to_s if id>-1
		@password = Gtk::Entry.new
		@password.visibility = false
		@password.text = window.config_db.bases[id]["password"].to_s if id>-1
		@chemin_documents = Gtk::Entry.new
		@chemin_documents.text = window.config_db.bases[id]["chemin_documents"].to_s if id>-1
		
		table = Gtk::Table.new 3, 3, false
		
		i=0
		table.attach Gtk::Label.new( "Nom de la connexion" ), 0, 1, i, i+1
		table.attach @nom, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Hôte" ), 0, 1, i, i+1
		table.attach @host, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Base de donnée" ), 0, 1, i, i+1
		table.attach @database, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Port" ), 0, 1, i, i+1
		table.attach @port, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Nom d'utilisateur" ), 0, 1, i, i+1
		table.attach @username, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Mot de passe" ), 0, 1, i, i+1
		table.attach @password, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Chemin vers les documents" ), 0, 1, i, i+1
		table.attach @chemin_documents, 1, 2, i, i+1
  	
  	frame = Gtk::Frame.new
		frame.label = (id<0 ? "Nouvelle base de données PostgreSQL" : "Modification de la base de données PostgreSQL")
		frame.add table
		
		self.vbox.pack_start frame, true, true, 3
		
		self.vbox.show_all

	end

end
