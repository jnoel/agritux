# coding: utf-8

class Tiers_box < Gtk::VBox

	attr_reader :tiers

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate true }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouveau tiers"
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, false, false, 3 ) unless @dial
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@nom = Gtk::Entry.new
		@type_model = Gtk::TreeStore.new(Integer, String)
		@type_cb = Gtk::ComboBox.new @type_model
		@tarif_model = Gtk::TreeStore.new(Integer, String)
		@tarif_cb = Gtk::ComboBox.new @tarif_model
		@adresse = Gtk::TextView.new
		@cp = Gtk::Entry.new
		@ville = Gtk::Entry.new
		@tel = Gtk::Entry.new
		@fax = Gtk::Entry.new
		renderer_left = Gtk::CellRendererText.new
		
		# Propriétés
		@adresse.wrap_mode = Gtk::TextTag::WRAP_WORD
		@adresse.set_size_request(10, 60)
		renderer_left.xalign = 0
		
		# Agencement		
		vbox = Gtk::VBox.new false, 3
		
		table = Gtk::Table.new 3, 9, false
		
		table.attach( Gtk::Label.new("Nom :"), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @nom, 1, 4, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		@type_cb.pack_start renderer_left, true
		@type_cb.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new("Client/Fournisseur :"), 4, 5, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @type_cb, 5, 8, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		scroll_adresse = Gtk::ScrolledWindow.new
		scroll_adresse.add @adresse
		scroll_adresse.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_adresse.shadow_type = Gtk::SHADOW_IN
		
		table.attach( Gtk::Label.new("Adresse :"), 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( scroll_adresse, 1, 8, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new("Code postal :"), 0, 1, 2, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @cp, 1, 2, 2, 3, Gtk::FILL, Gtk::FILL, 2, 3 )
		
		table.attach( Gtk::Label.new("Ville :"), 2, 3, 2, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @ville, 3, 4, 2, 3, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new("Tél :"), 4, 5, 2, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @tel, 5, 6, 2, 3, Gtk::FILL, Gtk::FILL, 2, 3 )
		
		table.attach( Gtk::Label.new("Fax :"), 6, 7, 2, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @fax, 7, 8, 2, 3, Gtk::FILL, Gtk::FILL, 2, 3 )
		
		vbox.pack_start table, false, false, 3
		
		vbox.pack_start notebook, true, true, 3
		
		vbox
	
	end
	
	def notebook
	
		# Objets	
		@portable = Gtk::Entry.new
		@mail = Gtk::Entry.new
		@site = Gtk::Entry.new
		@note = Gtk::TextView.new
		envoi_mail = Gtk::Button.new
		envoi_mail2 = Gtk::Button.new
		visite_site = Gtk::Button.new
		@ged = Ged_box.new @window, "tiers"
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new
		table_info = Gtk::Table.new 2, 4, false
		scroll_info = Gtk::ScrolledWindow.new

		# Propriétés des objets
		@note.wrap_mode = Gtk::TextTag::WRAP_WORD
		scroll_note.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_note.shadow_type = Gtk::SHADOW_NONE
		scroll_info.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_info.shadow_type = Gtk::SHADOW_NONE
		envoi_mail.image = Gtk::Image.new "resources/icons/saisie.png"
		envoi_mail.signal_connect("clicked") { mailer(@mail.text) unless @mail.text.empty? }
		envoi_mail.tooltip_text = "Ecrire un mail"
		envoi_mail2.image = Gtk::Image.new "resources/icons/saisie.png"
		envoi_mail2.signal_connect("clicked") { mailer(@mail2.text) unless @mail2.text.empty? }
		envoi_mail2.tooltip_text = "Ecrire un mail"
		visite_site.image = Gtk::Image.new "resources/icons/internet.png"
		visite_site.signal_connect("clicked") { naviguer unless @site.text.empty? }
		visite_site.tooltip_text = "Visiter le site"
		
		# Les labels
		label_mail = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail.add Gtk::Label.new "Email :"
		label_mail2 = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail2.add Gtk::Label.new "Email secondaire :"
		label_site = Gtk::Alignment.new 0, 0.5, 0, 0
		label_site.add Gtk::Label.new "Site web :"
		label_portable = Gtk::Alignment.new 0, 0.5, 0, 0
		label_portable.add Gtk::Label.new "Tél. portable :"
		
		# Agencement du conteneur Informations
		i=0
		table_info.attach( label_mail , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @mail, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( envoi_mail, 2, 3, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_site , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @site, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( visite_site, 2, 3, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_portable , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @portable, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		scroll_info.add_with_viewport table_info
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du notebook
		notebook.append_page scroll_info, Gtk::Label.new("Informations")
		notebook.append_page scroll_note, Gtk::Label.new("Notes")
		#notebook.append_page @ged, Gtk::Label.new("GED")

		# Renvoie
		notebook
		
	end
	
	def refresh id=0
	
		@id = id
		@nom.text = ""
		@type_cb.active = 0
		@adresse.buffer.text = ""
		@cp.text = ""
		@ville.text = ""
		@note.buffer.text = ""
		@tel.text = ""
		@fax.text = ""
		@tiers = nil
		@mail.text = ""
		@site.text = ""
		@portable.text = ""
		
		if id>0 then
			@frame.label = "Tiers n° #{id}"
			
			@tiers = Tiers.find(id)
			
			if @tiers then
				@nom.text = @tiers.nom.to_s
				@type_cb.active = -1
				@adresse.buffer.text = @tiers.adresse.to_s
				@cp.text = @tiers.cp.to_s
				@ville.text = @tiers.ville.to_s
				@note.buffer.text = @tiers.note.to_s
				remplir_type @tiers.typetiers.id
				@tel.text = @tiers.tel.to_s
				@fax.text = @tiers.fax.to_s
				@mail.text = @tiers.mail.to_s
				@site.text = @tiers.site.to_s
				@portable.text = @tiers.portable.to_s
			else
				@window.message_erreur tiers.error
			end
			
		else
			@tiers = Tiers.new
			@frame.label = "Nouveau Tiers"
			remplir_type
		end
		
		@ged.refresh @id, @mail.text
	
	end
	
	def validate quitter=false
		
		res = false
		if !@nom.text.empty? then
			date_modif = Date.today.strftime("%Y-%m-%d")
			type_id = @type_model.get_value(@type_cb.active_iter,0).nil? ? 1 : @type_model.get_value(@type_cb.active_iter,0)
		
			@tiers.nom = @nom.text
			p @type_cb.active
			@tiers.typetiers_id = type_id
			@tiers.adresse = @adresse.buffer.text
			@tiers.cp = @cp.text
			@tiers.ville = @ville.text
			@tiers.note = @note.buffer.text
			@tiers.tel = @tel.text
			@tiers.fax = @fax.text
			@tiers.mail = @mail.text
			@tiers.site = @site.text
			@tiers.portable = @portable.text
						
			res = @tiers.save
		
			if res then
				quit if (!@dial and quitter)
			else
				@window.message_erreur res.error
			end
		else
			@window.message_erreur "Vous devez saisir un nom pour ce tiers !"
			res = false
		end
		return res
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def remplir_type type=nil
		
		@type_cb.model.clear
		
		typetiers = Typetiers.order(:id)
		
		if typetiers then
			i=0
			id=-1
			typetiers.each do |t|
				iter = @type_model.append nil
				iter[0] = t.id
				iter[1] = t.designation
				if !type.nil? then 
					id = i if type.eql?(t.id) 
				end
				i += 1
			end
			@type_cb.active = ( type.nil? ? -1 : id )
		else
			@window.message_erreur typetiers.error
		end
		
	end
	
	def mailer mail
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-email #{mail}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def naviguer
		
		if !@site.text.include?("http://")
			@site.text = "http://" + @site.text
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open #{@site.text}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def quit statut=nil
	
		@window.liste_tiers.refresh #unless statut.nil?
		@window.affiche @window.liste_tiers
		@window.liste_tiers.focus
	
	end
	
end
