# coding: utf-8

class DialNewGed < Gtk::Dialog

	attr_reader :nom
	
	def initialize window
		
		super("Nouveau document", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		#set_default_size 900, 300
		
		table = Gtk::Table.new 3, 2, false
		
		label_nom = Gtk::Alignment.new 0, 0.5, 0, 0
		label_nom.add Gtk::Label.new("Nom et extension du fichier:")
		table.attach( label_nom, 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		@nom = Gtk::Entry.new 
		table.attach( @nom, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		
		@nom.signal_connect( "activate" ) {
			response(Gtk::Dialog::RESPONSE_OK)
		}
		
		self.vbox.pack_start table, true, true, 3
		
		self.vbox.show_all
	
	end

end		
