# coding: utf-8

class ListeSubventions_box < Gtk::VBox

	attr_reader :view, :search_gencode, :add_new
	
	def initialize window, research = false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@research = research
		
		@position = [0,0]
		 
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new t.grant.new
		@add_new.add hboxajout
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add @add_new
		frame = Gtk::Frame.new t.global.grants
		@compteur = Gtk::Label.new 
		
		hbox = Gtk::HBox.new false, 3
		@search_gencode = Gtk::Entry.new
		@search_gencode.primary_icon_stock = Gtk::Stock::FIND
		@search_gencode.secondary_icon_stock = Gtk::Stock::CLEAR
		
		# Agencement
		hbox.add @search_gencode
		
		hbox.add align_button
		
		vbox.pack_start hbox, false, false, 3 
		vbox.pack_start tableau, true, true, 3 
		vbox.pack_start @compteur, false, false, 3 
		@next = Gtk::Button.new ">"
		align_next = Gtk::Alignment.new 1, 1, 0, 0
		align_next.add @next
		frame.add vbox
		
		if !@research
			@add_new.signal_connect( "clicked" ) {
				@window.subvention.refresh
				@window.affiche @window.subvention
			}
		end
		
		@search_gencode.signal_connect('activate') {
			refresh @search_gencode.text
		}
		
		add frame
	
	end
	
	def tableau
	
		# list_store (id, designation, date, date_str, montant, montant_tri)
		@list_store = Gtk::TreeStore.new(Integer, String, Date, String, Float, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				@window.subvention.refresh @view.model.get_value @view.selection.selected, 0
				@window.affiche @window.subvention
				@window.subvention.focus
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_center = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new(t.global.designation, renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.date, renderer_right, :text => 3)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.amount, renderer_center, :text => 5)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh search=nil
		
		subventions = Subvention.order("date")
		if search
			sub = Subvention.arel_table
			subventions = subventions.where(sub[:designation].matches("%#{search}%"))
		end
		
		@compteur.text = "#{t.global.count_elements} : #{subventions.count}"

		if subventions then	
			remplir subventions
		end	
	
	end
	
	def remplir res

		@list_store.clear
		
		res.each { |h| 	
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.designation.to_s
			iter[2] = h.date
			iter[3] = h.date.strftime("%d/%m/%Y")
			iter[4] = h.montant
			iter[5] = "%.2f #{t.global.currency}" % h.montant.to_f
		}
			
	end
	
	def focus
		@search_gencode.grab_focus
	end
	
end

