#!/usr/bin/env ruby
# coding: utf-8

#
# Need of scanimage (sane), and convert (imagemagik) installed on system
#

class Scan
	
	def initialize conf
		@conf = conf.conf
		@chemin_documents = @conf["chemin_documents"] if @conf["chemin_documents"]
		@scanner = @conf["scanner"] if @conf["scanner"]
		@resolution = @conf["resolution"].to_i if @conf["resolution"]
	end

	def scanner mode, page=1
		p `/usr/bin/scanimage -d "#{@scanner}" --resolution #{@resolution}dpi -l 0mm -t 0mm -x 210mm -y 297mm --mode #{mode} > "scan#{page}.pnm"`
	end
	
	def creer_dossier id, type
		if File.directory?(@chemin_documents)
			chemin = "#{@chemin_documents}/#{type}/"
			Dir.mkdir(chemin) unless File.directory?(chemin)
			chemin += "#{id}/"
			Dir.mkdir(chemin) unless File.directory?(chemin)
			return chemin
		else
			p "Erreur du chemin de base"
			return nil
		end
	end

	def convertir chemin, page=1
		liste = ""
		(1..page).each do |p|
			p `pnmtojpeg --quality=50 scan#{p}.pnm > scan#{p}.jpg`
			if p.eql?(1)
				miniature = "#{File.dirname(chemin)}/#{File.basename(chemin, File.extname(chemin))}_miniature.jpg"
				p `convert scan#{p}.jpg -resize 80x80 #{miniature}`
			end
			liste += "scan#{p}.jpg "
		end
		p `convert -depth #{@resolution} -resize 1000x1500 #{liste} #{chemin}`

	end

	def nettoyer page=1
		(1..page).each do |p|
			File.delete("scan#{p}.pnm") if File.exist?("scan#{p}.pnm")
			File.delete("scan#{p}.jpg") if File.exist?("scan#{p}.jpg")
		end
	end
	
	def renommer nom, chemin, ext
		nom_def = nom
		i=0
		while File.exist?("#{chemin}#{nom_def}#{ext}") do
			i += 1
			nom_def = nom + i.to_s
		end
		nom_def
	end

	def acquisition nom, id, type, ext=".pdf", mode="Gray"
		chemin = creer_dossier id, type
		if chemin
			nom_def = renommer nom, chemin, ext
			page = 1
			nouvelle_page = true
			while nouvelle_page do
				scanner mode, page
				dialog = Gtk::MessageDialog.new(@window, 
		                            Gtk::Dialog::DESTROY_WITH_PARENT,
		                            Gtk::MessageDialog::QUESTION,
		                            Gtk::MessageDialog::BUTTONS_YES_NO,
		                            "Voulez-vous ajouter une autre page à ce document ?")
				response = dialog.run
				case response
					when Gtk::Dialog::RESPONSE_NO
						nouvelle_page = false
					else
						page += 1
				end 
				dialog.destroy
			end
			
			convertir "#{chemin}#{nom_def}#{ext}", page
			nettoyer page
			if type.eql?("tiers")
				ged = Gedtiers.new
				ged.tiers_id = id
			elsif type.eql?("documents")
				ged = Geddocument.new
				ged.document_id = id
			elsif type.eql?("articles")
				ged = Gedarticle.new
				ged.article_id = id
			elsif type.eql?("comptes")
				ged = Gedcompte.new
				ged.compte_id = id
			end
			ged.fichier = nom_def+ext
			ged.save
		end
	end
	
end
