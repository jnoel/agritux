# coding: utf-8

class Ged_box < Gtk::HBox

	attr_accessor :liste, :table

	def initialize window, type="documents" 
		
		super(false, 3)
		
		@window = window
		@type = type
		
		scan = Gtk::Button.new
		hbox_scan = Gtk::HBox.new false, 2
		hbox_scan.add Gtk::Image.new( "./resources/icons/scanner.png" )
		hbox_scan.add Gtk::Label.new "Scanner"
		scan.add hbox_scan
		scan.signal_connect("clicked") { scanner }
		
		suppr_ged = Gtk::Button.new
		hbox_suppr_ged = Gtk::HBox.new false, 2
		hbox_suppr_ged.add Gtk::Image.new( "./resources/icons/trash-full.png" )
		hbox_suppr_ged.add Gtk::Label.new "Supprimer"
		suppr_ged.add hbox_suppr_ged
		suppr_ged.signal_connect("clicked") { supprime }
		
		ajout_ged = Gtk::Button.new
		hbox_ajout_ged = Gtk::HBox.new false, 2
		hbox_ajout_ged.add Gtk::Image.new( "./resources/icons/add.png" )
		hbox_ajout_ged.add Gtk::Label.new "Ajouter"
		ajout_ged.add hbox_ajout_ged
		ajout_ged.signal_connect("clicked") { ajout }
		
		nouveau_ged = Gtk::Button.new
		hbox_nouveau_ged = Gtk::HBox.new false, 2
		hbox_nouveau_ged.add Gtk::Image.new( "./resources/icons/document-new.png" )
		hbox_nouveau_ged.add Gtk::Label.new "Nouveau"
		nouveau_ged.add hbox_nouveau_ged
		nouveau_ged.signal_connect("clicked") { nouveau }
		
		vbox = Gtk::VBox.new false, 2
		
		vbox.pack_start scan, false, false, 3
		vbox.pack_start ajout_ged, false, false, 3
		vbox.pack_start nouveau_ged, false, false, 3
		#vbox.pack_start suppr_ged, false, false, 3
		
		self.pack_start( vbox, false, false, 3 )
		self.pack_start( table_ged, true, true, 3 )
		
	end
	
	def table_ged
									# ID     Fichier  Miniature    Size   Sizetri  date    datetri
		@liste = Gtk::ListStore.new(Integer, String, Gdk::Pixbuf, String, Integer, String, String)
		@table = Gtk::TreeView.new(@liste)
		
		@table.signal_connect ("row-activated") { |view, path, column|
			ouvrir
		}
		
		@table.signal_connect ("drag-drop") { |widget, drag_context, x, y, time|
			p time
		}
		
		@table.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @table.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@table.set_cursor(npath[0], nil, false)
					menu_ged					
				end
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_right = Gtk::CellRendererText.new
		renderer_left.background = renderer_right.background = "white"
		renderer_left.yalign = renderer_right.yalign = 0.5
		renderer_left.xalign = 0
		renderer_right.xalign = 1
		
		col = Gtk::TreeViewColumn.new("ID", renderer_left, :text => 0)
		#@table_ged.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Fichier", renderer_left, :text => 1)
		col.expand = true
		col.sort_column_id = 1
		@table.append_column(col)	
		
		renderer_pix = Gtk::CellRendererPixbuf.new
		col = Gtk::TreeViewColumn.new
		col.title = "Aperçu"
		col.pack_start(renderer_pix, true)
		col.add_attribute(renderer_pix, 'pixbuf', 2)
		@table.append_column(col)	
		
		col = Gtk::TreeViewColumn.new("Taille", renderer_right, :text => 3)
		col.sort_column_id = 4
		@table.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date", renderer_right, :text => 5)
		col.sort_column_id = 6
		@table.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(Gtk::POLICY_NEVER,Gtk::POLICY_AUTOMATIC)
    	scroll.add @table
		
		scroll
	
	end
	
	def refresh id, mail=nil
		
		@id = id
		
		@mail = mail
		
		p mail
		
		@liste.clear
		if @type.eql?("documents")
			ged = Geddocument.where(:document_id => id).order(:fichier)
		elsif @type.eql?("tiers")
			ged = Gedtiers.where(:tiers_id => id).order(:fichier)
		elsif @type.eql?("cultures")
			ged = Gedculture.where(:culture_id => id).order(:fichier)
		elsif @type.eql?("articles")
			ged = Gedarticle.where(:article_id => id).order(:fichier)
		elsif @type.eql?("comptes")
			ged = Gedcompte.where(:compte_id => id).order(:fichier)
		end
		ged.each { |g|
			iter = @liste.append
			iter[0] = g.id
			iter[1] = g.fichier
			miniature = "#{File.basename(g.fichier, File.extname(g.fichier))}_miniature.jpg"
			chemin_mini = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{id}/#{miniature}"
			chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{id}/#{g.fichier}"
			if File.exist?(chemin_mini)
				iter[2] = Gdk::Pixbuf.new(chemin_mini, 25, 40)
			else
				begin
					iter[2] = Gdk::Pixbuf.new(chemin, 25, 40)
				rescue
				end
			end
			taille = File.size(chemin) if File.exist?(chemin)
			iter[3] = ("%.1f ko" % (taille/1000) ) if File.exist?(chemin)
			iter[4] = taille if File.exist?(chemin)
			iter[5] = g.created_at.strftime("%d/%m/%Y")
			iter[6] = g.created_at.strftime("%Y-%m-%d")
		}
		
	end
	
	def menu_ged
	
		ouvrir_tb = Gtk::MenuItem.new "Ouvrir la pièce"
		ouvrir_tb.signal_connect( "activate" ) { 
			ouvrir
		}
		
		mail_tb = Gtk::MenuItem.new "Envoyer la pièce par e-mail"
		mail_tb.signal_connect( "activate" ) { 
			mailer @mail
		}
		
		suppr_tb = Gtk::MenuItem.new "Supprimer la pièce"
		suppr_tb.signal_connect( "activate" ) { 
			supprime
		}
		
		menu = Gtk::Menu.new
		menu.append ouvrir_tb
		menu.append mail_tb
		menu.append suppr_tb
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def ouvrir
		fichier = @liste.get_value(@table.selection.selected, 1)
		chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{@id}/#{fichier}"
		if File.exist?(chemin)
			system "xdg-open", chemin
		else
			@window.message_erreur "Le fichier est inaccessible... Veuillez vérifier la configuration du chemin de la GED."
		end
	end
	
	def mailer mail
		fichier = @liste.get_value(@table.selection.selected, 1)
		chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{@id}/#{fichier}"
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-email --attach '#{chemin}' #{mail.to_s}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def scanner
		if validate
			dial_scan = DialScan.new @window
			dial_scan.nom.text = @window.document.doc.ref2 if @type.eql?("documents")
			dial_scan.run { |response| 
				if response.eql?(-5)
					nom = (dial_scan.nom.text.empty? ? "scan" : dial_scan.nom.text.gsub(" ","_") )
					mode = dial_scan.mode.active_text
					@window.scan.acquisition nom, @id, @type, ext=".pdf", mode
					refresh @id
				end
				dial_scan.destroy 			
			}
		end
	end
	
	def validate
	
		res = false
		if @type.eql?("documents")
			res = @window.document.validate quitter=false
		elsif @type.eql?("tiers")
			res = @window.tiers.validate quitter=false
		elsif @type.eql?("articles")
			res = @window.article.validate quitter=false
		elsif @type.eql?("comptes")
			res = @window.compte.validate quitter=false
		end
		res
	
	end
	
	def ajout
		if validate
			dialog = Gtk::FileChooserDialog.new("Open File",
						                         @window,
						                         Gtk::FileChooser::ACTION_OPEN,
						                         nil,
						                         [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
						                         [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])

			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				fichier = dialog.filename
				chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{@id}/#{File.basename(fichier)}"
				# on renomme si un fichier du meme nom existe déjà
				#chemin = @window.scan.renommer File.basename(fichier, File.extname(fichier)), chemin, File.extname(fichier)
				# on créer les dossiers si ils n'existent pas déjà
				@window.scan.creer_dossier @id, @type
				# on fait la copie de fichier
				FileUtils.cp fichier, chemin
				# Puis on créé une entrée en base de donnée pour la ged
				if @type.eql?("tiers")
					ged = Gedtiers.new
					ged.tiers_id = @id
				elsif @type.eql?("documents")
					ged = Geddocument.new
					ged.document_id = @id
				elsif @type.eql?("articles")
					ged = Gedarticle.new
					ged.article_id = @id
				elsif @type.eql?("comptes")
					ged = Gedcompte.new
					ged.compte_id = @id
				end
				ged.fichier = File.basename(fichier)
				ged.save
				refresh @id
			end
			dialog.destroy
		end
	end
	
	def nouveau
		if validate
			dial_new = DialNewGed.new @window
			dial_new.run { |response| 
			if response.eql?(-5)
				fichier = (dial_new.nom.text.empty? ? "scan" : dial_new.nom.text.gsub(" ","_") )
				chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{@id}/#{File.basename(fichier)}"
				File.open(chemin, 'w')
				# Puis on créé une entrée en base de donnée pour la ged
				if @type.eql?("tiers")
					ged = Gedtiers.new
					ged.tiers_id = @id
				elsif @type.eql?("documents")
					ged = Geddocument.new
					ged.document_id = @id
				elsif @type.eql?("articles")
					ged = Gedarticle.new
					ged.article_id = @id
				elsif @type.eql?("comptes")
					ged = Gedcompte.new
					ged.compte_id = @id
				end
				ged.fichier = File.basename(fichier)
				ged.save
				# Et on ouvre le document avec l'application par défaut
				if File.exist?(chemin)
					system "xdg-open", chemin
				else
					@window.message_erreur "Le fichier est inaccessible... Veuillez vérifier la configuration du chemin de la GED."
				end
					refresh @id
				end
			dial_new.destroy 			
			}
		end
	end
	
	# Suppression d'un fichier de la GED
	def supprime
		if @table.selection.selected
			dialog = Gtk::MessageDialog.new(@window, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::QUESTION,
                                Gtk::MessageDialog::BUTTONS_YES_NO,
                                "Voulez-vous réellement supprimer ce fichier de la GED ?")
			response = dialog.run
			case response
			  when Gtk::Dialog::RESPONSE_YES
				id = @liste.get_value(@table.selection.selected, 0)
				if @type.eql?("documents")
					res = Geddocument.delete(id)
				elsif @type.eql?("tiers")
					res = Gedtiers.delete(id)
				elsif @type.eql?("articles")
					res = Gedarticle.delete(id)
				elsif @type.eql?("comptes")
					res = Gedcompte.delete(id)
				end
				if res
					fichier = @liste.get_value(@table.selection.selected, 1)
					chemin = "#{@window.config_db.conf["chemin_documents"]}/#{@type}/#{@id}/#{fichier}"
					File.delete(chemin) if File.exist?(chemin)
					refresh @id
				end
			end 
			dialog.destroy
			
		end
	end
	
end

