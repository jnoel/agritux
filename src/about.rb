# coding: utf-8
		
class About < Gtk::AboutDialog

	def initialize version
		
		super()

		self.version = version
		self.comments = t.about.comments
		lic = ""
		file = File.open("../LICENSE", "r")
		while (line=file.gets)
			lic += line
		end
		file.close
		self.license = lic
		self.website = "http://www.agritux.net"
		self.authors = ["Jean-Noël Rouchon", "Bernard Mondon"]
		self.artists = ["OpenClipart", "Faience icon theme"]
		self.translator_credits = "Fouad Yammine"
		self.logo = Gdk::Pixbuf.new( "./resources/icons/agritux128.png" )
		self.set_icon( "./resources/icons/agritux128.png" )
		
		self.show
	
	end

end		
