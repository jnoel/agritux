# coding: utf-8

class RapportExercices < Gtk::TreeView

  attr_reader :serie
  
  def initialize window
       
  	super( Gtk::ListStore.new(String, String, String, String) )
  	
  	@serie = []
  	
		@list_store = self.model
		
		self.signal_connect ("row-activated") { |view, path, column|
			annee = self.model.get_value(self.selection.selected, 0)
			dial_detail = DialDetailRapport.new annee, window
			dial_detail.run { |response| 
				dial_detail.destroy 			
			}
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_green = Gtk::CellRendererText.new
		renderer_green.background = "#e7ffe7"
		renderer_green.xalign = 1
		renderer_green.yalign = 0
		
		# Create a renderer with the background property set
		renderer_red = Gtk::CellRendererText.new
		renderer_red.background = "#ffe7e7"
		renderer_red.xalign = 1
		renderer_red.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right_bold = Gtk::CellRendererText.new
		renderer_right_bold.xalign = 1
		renderer_right_bold.yalign = 0
		renderer_right_bold.background = "#eee"
		
		col = Gtk::TreeViewColumn.new(t.global.year, renderer_left, :text => 0)
		col.expand = true
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.dashboard.outgoings, renderer_red, :text => 1)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.dashboard.incomes, renderer_green, :text => 2)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.dashboard.earnings, renderer_right_bold, :text => 3)
		self.append_column(col)
	
	end
	
	def refresh an_min, an_max
		
		@serie = []

		@list_store.clear
				
		i = 1
		(an_min..an_max).to_a.reverse.each {|a|
				
			date_deb = Date.parse("01/01/#{a}")
			date_fin = Date.parse("31/12/#{a}")
			
			recettes = Cultureligne.where(:date => date_deb..date_fin, :article_id => -1).sum(:prix_total)
			subventions = Subvention.where(:date => date_deb..date_fin).sum(:montant)
			depenses = Articleentree.where("date>='#{date_deb}' AND date<='#{date_fin}' AND article_id>0" ).sum(:prix_total)
	
			iter = @list_store.append
			iter[0] = a.to_s
			iter[1] = "%.2f #{t.global.currency}" % depenses
			iter[2] = "%.2f #{t.global.currency}" % (recettes+subventions)
			iter[3] = "%.2f #{t.global.currency}" % (recettes+subventions-depenses)
			
			@serie = @serie.unshift [depenses, recettes+subventions, recettes+subventions-depenses] if i<5
			i+=1
			
		}
		
	end
	
end
