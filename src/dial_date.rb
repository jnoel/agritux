# coding: utf-8

class DialDate < Gtk::Dialog

	attr_reader :date
	
	def initialize window
		
		super("Choisir une date", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		#set_default_size 900, 300
		
		@date = Gtk::Calendar.new
		
		#@date.signal_connect( "activate" ) {
		#	response(Gtk::Dialog::RESPONSE_OK)
		#}
		
		self.vbox.pack_start @date, true, true, 3
		
		self.vbox.show_all
	
	end

end		

