# coding: utf-8

class Cycle_box < Gtk::VBox

	def initialize window, historique=false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		
		@culture_id = nil
		@historique=historique
		
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		vbox.pack_start( en_tete, false, false, 3 )
		vbox.pack_start( tableau, true, true, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@parcelle_model = Gtk::TreeStore.new(Integer, String)
		@parcelle = Gtk::ComboBox.new @parcelle_model
		renderer_left = Gtk::CellRendererText.new
		@parcelle.pack_start renderer_left, true
		@parcelle.add_attribute renderer_left, "text", 1
		@parcelle.signal_connect('changed') {
			change_parcelle
		}
		
		#Conteneur
		hbox = Gtk::HBox.new false, 3
		hbox.pack_start Gtk::Label.new(t.farming.choose_parcel), false, false, 3
		hbox.pack_start @parcelle, true, true, 3
		
		hbox
	
	end
	
	def tableau
		
		# 															id      cycle    date date_string article_id article unite quantite prix_unitaire prix_total image_note trash, raison, methode,notes)
		@list_store = Gtk::TreeStore.new(Integer, Integer, Date, String, Integer, String, String, String, String, String, Gdk::Pixbuf, Gdk::Pixbuf, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		# Colonne pour le numéro de cycle :
		renderer_cycle = Gtk::CellRendererSpin.new
		renderer_cycle.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_cycle.digits = 0
		renderer_cycle.xalign = 1
		renderer_cycle.yalign = 0.5
		renderer_cycle.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_cycle.editable = true
		renderer_cycle.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			if text.to_i>0
				ligne[1] = text.to_i
				save_ligne ligne
			end
		} 
		col = Gtk::TreeViewColumn.new(t.global.cycle, renderer_cycle, :text => 1)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.date, renderer_left, :text => 3)
		col.resizable = true
		@view.append_column(col)
		
		renderer_article = Gtk::CellRendererCombo.new
		renderer_article.has_entry = false
		renderer_article.xalign = 0
		renderer_article.yalign = 0
		renderer_article.editable = true		
		renderer_article.model = modele_article
		renderer_article.text_column = 1
		renderer_article.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			add_article ligne, text
		} 
		col = Gtk::TreeViewColumn.new(t.global.good, renderer_article, :text => 5)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		#renderer_unite = Gtk::CellRendererCombo.new
		renderer_unite = Gtk::CellRendererText.new
		#renderer_unite.has_entry = false
		renderer_unite.xalign = 0.5
		renderer_unite.yalign = 0.5
		#renderer_unite.editable = true		
		#renderer_unite.model = modele_unite
		#renderer_unite.text_column = 0
		#renderer_unite.signal_connect('edited') { |combo, data, text|
		#	ligne = @view.model.get_iter(data)
		#	ligne[6] = text
		#	save_ligne ligne
			#
		#} 
		col = Gtk::TreeViewColumn.new(t.global.unit, renderer_unite, :text => 6)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour la quantité :
		renderer_quantite = Gtk::CellRendererSpin.new
		renderer_quantite.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_quantite.digits = 2
		renderer_quantite.xalign = 1
		renderer_quantite.yalign = 0
		renderer_quantite.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_quantite.editable = true
		renderer_quantite.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			change_quantite ligne, text
		}
		col = Gtk::TreeViewColumn.new(t.global.quantity, renderer_quantite, :text => 7)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour le PU :
		renderer_pu = Gtk::CellRendererSpin.new
		renderer_pu.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_pu.digits = 2
		renderer_pu.xalign = 1
		renderer_pu.yalign = 0.5
		renderer_pu.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_pu.editable = true
		renderer_pu.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			ligne[8] = "%.2f" % text.to_f
			ligne[9] = "%.2f" % (ligne[7].to_f*ligne[8].to_f).round(2)
			save_ligne ligne
		} 
		col = Gtk::TreeViewColumn.new(t.global.unit_price, renderer_pu, :text => 8)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.total, renderer_right, :text => 9)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour la note
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0.5
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 10)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 11)
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	# Les événements
		@view.signal_connect ("button-release-event") { |tree,event|
			# Si clic gauche
			e = npath = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				#si clic sur colonne supprimer
				if colonne.eql?(@view.get_column(8))
					ligne = @view.model.get_iter(e[0])
					suppr_ligne ligne
				#si clic sur colonne commentaire
				elsif colonne.eql?(@view.get_column(7))
					ligne = @view.model.get_iter(e[0])
					commenter_ligne ligne
				#si clic sur colonne date
				elsif colonne.eql?(@view.get_column(1))
					ligne = @view.model.get_iter(e[0])
					changer_date ligne
				end
			end
		}
  	
  	scroll
		
	end
	
	def modele_article
		articles = Article.order(:id)
		article_model = Gtk::ListStore.new(Integer, String)
		articles.each { |a|
			iter = article_model.append
			iter[0] = a.id
			iter[1] = a.designation
		}
		iter = article_model.append
		iter[0] = -1
		iter[1] = "Vente"
		iter = article_model.append
		iter[0] = -1
		iter[1] = "Subventions"
		unless @historique
			iter = article_model.append
			iter[0] = -2
			iter[1] = "Fin de cycle"
		end
		article_model	
	end
	
	def modele_unite
		unite_model = Gtk::ListStore.new(String)
		units = @window.unites
		units.each { |a|
			iter = unite_model.append
			iter[0] = a
		}
		unite_model
	end
	
	def add_article ligne, text, qtite=1.0
		unless text.nil?
			continu = false
			if text.eql?("Fin de cycle")
				dialog = Gtk::MessageDialog.new(@window, 
						                        Gtk::Dialog::DESTROY_WITH_PARENT,
						                        Gtk::MessageDialog::QUESTION,
						                        Gtk::MessageDialog::BUTTONS_YES_NO,
						                        t.farming.confirm_end_cycle)
				response = dialog.run
				case response
					when Gtk::Dialog::RESPONSE_YES
					continu = true
				end 
				dialog.destroy
			else
				continu = true
			end
			
			if continu
				
				#Si changement sur ancien article, modification du stock
				if ligne[0]>0
					change_stock ligne[4], ligne[7].to_f
				end
			
				article = ((text.eql?("Vente") or text.eql?("Fin de cycle") or text.eql?("Subventions")) ? nil : Article.where(:designation => text).first)
				if ligne[5].nil?
					iter = @list_store.append nil
					iter[1] = @cycle_en_cours
					ligne[10] = Gdk::Pixbuf.new("./resources/icons/document-new.png", 24, 24)
					ligne[11] = Gdk::Pixbuf.new("./resources/icons/trash-full.png", 24, 24)
					ligne[2] = DateTime.now
					ligne[3] = ligne[2].strftime("%d/%m/%Y")
				end
				ligne[5] = text
				ligne[4] = (article ? article.id : ((text.eql?("Vente") or text.eql?("Subventions")) ? -1 : -2))
				ligne[6] = (article ? article.unite : "unité")
				ligne[7] = "%.2f" % qtite
				if article
					ligne[8] = "%.2f" % article.prix
					ligne[9] = "%.2f" % (article.prix*qtite)
				else
					if text.eql?("Vente")
						culture = Culture.find(@culture_id)
						ligne[8] = "%.2f" % culture.prix
						ligne[9] = "%.2f" % (culture.prix*qtite)
						ligne[6] = culture.unite
					end
					if text.eql?("Subventions")
						culture = Culture.find(@culture_id)
						ligne[8] = "%.2f" % culture.subventions
						ligne[9] = "%.2f" % (culture.subventions*qtite)
						ligne[6] = culture.unite
					end
				end
				ligne[1] = @cycle_en_cours+1 if ligne[4].eql?(-2)
				save_ligne ligne
				change_stock article.id, -1 if article
				refresh @culture_id if ligne[4].eql?(-2)
			end
		end
	end
	
	def save_ligne ligne
	
		if ligne[5]
			l = nil
			if ligne[0]>0
				l = Cultureligne.find(ligne[0])
			else
				l = Cultureligne.new
			end
			l.cycle = ligne[1]
			l.date = ligne[2]
			l.culture_id = @culture_id
			l.article_id = ligne[4]
			l.article_designation = ligne[5]
			l.parcelle_id = @parcelle_en_cours
			l.unite = ligne[6]
			l.quantite = ligne[7].to_f
			l.prix_unitaire = ligne[8].to_f
			l.prix_total = ligne[9].to_f
			l.save
			ligne[0] = l.id if ligne[0]<=0
		end
		
	end
	
	def suppr_ligne ligne
		
		unless ligne[5].nil?
			suppr = false
			dialog = Gtk::MessageDialog.new(@window, 
				                          Gtk::Dialog::DESTROY_WITH_PARENT,
				                          Gtk::MessageDialog::QUESTION,
				                          Gtk::MessageDialog::BUTTONS_YES_NO,
				                          t.farming.confirm_delete_line)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
				suppr = true
			end 
			dialog.destroy
	
			if suppr
				if ligne[0]>0
					l = Cultureligne.find(ligne[0])
					l.destroy
					change_stock ligne[4], ligne[7].to_f
				end
				@view.model.remove ligne
			end
		end
	
	end
	
	def commenter_ligne ligne
	
		unless ligne[5].nil?
			dial_commentaire = DialCommentaire.new @window
			dial_commentaire.raison.text = ligne[12]
			dial_commentaire.methode.text = ligne[13]
			dial_commentaire.notes.text = ligne[14]
			dial_commentaire.run { |response| 
				if response.eql?(-5)
					l = Cultureligne.find(ligne[0])
					l.raison = dial_commentaire.raison.text
					l.methode = dial_commentaire.methode.text
					l.note = dial_commentaire.notes.text
					l.save
				end
				dial_commentaire.destroy 			
			}
		end
		
	end
	
	def changer_date ligne
	
		unless ligne[5].nil?
			dial_date = DialDate.new @window
			dial_date.run { |response| 
				if response.eql?(-5)
					date = Date.parse "#{dial_date.date.year}-#{(dial_date.date.month+1)}-#{dial_date.date.day}"
					ligne[2] = date
					ligne[3] = date.strftime("%d/%m/%Y")
					save_ligne ligne
				end
				dial_date.destroy 			
			}
		end
	
	end
	
	def change_stock article_id, qtite
	
		if article_id>0
			article = Article.find(article_id)
			if article
				article.stock = article.stock+qtite
				article.save
			end
		end
	
	end
	
	def change_quantite ligne, text
	
		change_stock ligne[4], -(text.to_f-ligne[7].to_f)
		ligne[7] = "%.2f" % text.to_f
		ligne[9] = "%.2f" % (ligne[7].to_f*ligne[8].to_f).round(2)
		save_ligne ligne
	
	end
	
	def remplir_tableau res
	
		@list_store.clear
		
		res.each { |h| 	
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.cycle
			iter[2] = h.date
			iter[3] = h.date.strftime("%d/%m/%Y")
			iter[4] = h.article_id
			iter[5] = h.article_designation
			iter[6] = h.unite
			iter[7] = "%.2f" % h.quantite
			iter[8] = "%.2f" % h.prix_unitaire
			iter[9] = "%.2f" % h.prix_total
			iter[10] = Gdk::Pixbuf.new("./resources/icons/document-new.png", 24, 24)
			iter[11] = Gdk::Pixbuf.new("./resources/icons/trash-full.png", 24, 24)
			iter[12] = h.raison
			iter[13] = h.methode
			iter[14] = h.note
		}
		unless @historique
			iter = @list_store.append nil
			iter[1] = @cycle_en_cours
		end
	
	end
	
	def remplir_parcelle
		@parcelle_model.clear
		i = nil
		Parcelle.order(:id).each do |p|
			iter = @parcelle_model.append nil
			iter[0] = p.id
			iter[1] = p.designation
			i = iter if p.id.eql?(1)
		end
		@parcelle.active_iter = i if i
	end
	
	def change_parcelle
		@parcelle_en_cours = @parcelle.model.get_value(@parcelle.active_iter,0) if @parcelle.active_iter
		prepare
	end
	
	def refresh culture_id=0
		@culture_id = culture_id
		remplir_parcelle
	end
	
	def prepare
		@cycle_en_cours = 1
		if @culture_id>0 then
			max = Cultureligne.where(:culture_id => @culture_id, :parcelle_id => @parcelle_en_cours).maximum(:cycle)
			if max
				@cycle_en_cours = max
				if @historique
					lignes = Cultureligne.includes(:article).where(:cycle => (1..max-1), :culture_id => @culture_id, :parcelle_id => @parcelle_en_cours).where("article_id != -2").order(:date, :id)
				else
					lignes = Cultureligne.includes(:article).where(:cycle => @cycle_en_cours, :culture_id => @culture_id, :parcelle_id => @parcelle_en_cours).where("article_id != -2").order(:date, :id)
				end
				remplir_tableau lignes
			else
				@list_store.clear
				unless @historique
					iter = @list_store.append nil
					iter[1] = @cycle_en_cours
				end
			end
		else
			@list_store.clear
			unless @historique
				iter = @list_store.append nil
				iter[1] = @cycle_en_cours
			end
		end
	end
	
end

