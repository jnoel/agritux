# coding: utf-8

class Configuration_box < Gtk::VBox

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
				
		@window = window
		@login = window.login
		@dial = dial
		@chemin_config = @window.config_db.chemin_config
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = "Configuration"
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( notebook, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, false, false, 3 ) unless @dial
		
		self.add vbox
	
	end
	
	def notebook
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		
		# Agencement du notebook
		notebook.append_page societe, Gtk::Label.new("Société")
		notebook.append_page groupe_cultures, Gtk::Label.new("Groupes de cultures")
		#notebook.append_page groupe_articles, Gtk::Label.new("Groupes d' articles")
		#notebook.append_page ged, Gtk::Label.new("GED")

		# Renvoie
		notebook
		
	end
	
	def societe
		# Objets
		@language = Gtk::ComboBox.new Gtk::ListStore.new(String, String)
		@soc_nom = Gtk::Entry.new
		@soc_adresse = Gtk::TextView.new
		@soc_capital = Gtk::Entry.new
		@soc_tel = Gtk::Entry.new
		@soc_fax = Gtk::Entry.new
		@soc_mail = Gtk::Entry.new
		@soc_site = Gtk::Entry.new
		@soc_siret = Gtk::Entry.new
		@soc_rcs = Gtk::Entry.new
		@soc_naf = Gtk::Entry.new
		@soc_status = Gtk::Entry.new
		@soc_logo = Gtk::Image.new
		visite_site = Gtk::Button.new
		logo_btn = Gtk::Button.new "..."
		
		# Conteneurs
		table_info = Gtk::Table.new 2, 4, false
		scroll_info = Gtk::ScrolledWindow.new
		hbox_compte = Gtk::HBox.new false, 2
		
		# Les labels
		label_language = Gtk::Alignment.new 0, 0.5, 0, 0
		label_language.add Gtk::Label.new "Langue :"
		label_nom = Gtk::Alignment.new 0, 0.5, 0, 0
		label_nom.add Gtk::Label.new "Nom de la société :"
		label_adresse = Gtk::Alignment.new 0, 0.5, 0, 0
		label_adresse.add Gtk::Label.new "Adresse :"
		label_capital = Gtk::Alignment.new 0, 0.5, 0, 0
		label_capital.add Gtk::Label.new "Capital de :"
		label_tel = Gtk::Alignment.new 0, 0.5, 0, 0
		label_tel.add Gtk::Label.new "Tél. :"
		label_fax = Gtk::Alignment.new 0, 0.5, 0, 0
		label_fax.add Gtk::Label.new "Fax. :"
		label_mail = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail.add Gtk::Label.new "Email :"
		label_site = Gtk::Alignment.new 0, 0.5, 0, 0
		label_site.add Gtk::Label.new "Site web :"
		label_siret = Gtk::Alignment.new 0, 0.5, 0, 0
		label_siret.add Gtk::Label.new "SIRET :"
		label_rcs = Gtk::Alignment.new 0, 0.5, 0, 0
		label_rcs.add Gtk::Label.new "RCS :"
		label_naf = Gtk::Alignment.new 0, 0.5, 0, 0
		label_naf.add Gtk::Label.new "NAF :"
		label_status = Gtk::Alignment.new 0, 0.5, 0, 0
		label_status.add Gtk::Label.new "Status :"
		label_logo = Gtk::Alignment.new 0, 0.5, 0, 0
		label_logo.add Gtk::Label.new "Logo :"
		
		# Propriétés des objets
		scroll_info.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_info.shadow_type = Gtk::SHADOW_NONE
		visite_site.image = Gtk::Image.new "resources/icons/internet.png"
		visite_site.signal_connect("clicked") { naviguer unless @soc_site.text.empty? }
		visite_site.tooltip_text = "Visiter le site"
		logo_btn.signal_connect("clicked") { nouveau_logo }
		renderer_left = Gtk::CellRendererText.new
		@language.pack_start renderer_left, true
		@language.add_attribute renderer_left, "text", 1
		
		# Agencement du conteneur Informations
		i=0
		table_info.attach( label_language , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @language, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_status , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_status, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_nom , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_nom, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_adresse , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_adresse, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_tel , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_tel, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_fax , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_fax, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_mail , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_mail, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_site , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_site, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( visite_site, 2, 3, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_siret , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_siret, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_rcs , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_rcs, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_naf , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_naf, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_capital , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_capital, 1, 3, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		table_info.attach( label_logo , 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( @soc_logo, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		table_info.attach( logo_btn, 2, 3, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		scroll_info.add_with_viewport table_info
		
		scroll_info
	end
	
	def groupe_cultures
		
		groupe_model = Gtk::TreeStore.new(Integer, String, Integer)
		@groupe = Gtk::TreeView.new groupe_model
		
		@groupe.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @groupe.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@groupe.set_cursor(npath[0], nil, false)
					menu_groupe	false, true				
				else
					menu_groupe true, true
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Groupes", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		@groupe.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nb de cultures", renderer_left, :text => 2)
		col.resizable = true
		@groupe.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	
  	scroll.add_with_viewport @groupe
  	
  	scroll
	
	end
	
	def groupe_articles
		
		groupe_model = Gtk::TreeStore.new(Integer, String, Integer)
		@groupe2 = Gtk::TreeView.new groupe_model
		
		@groupe2.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @groupe2.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					#@groupe2.set_cursor(npath[0], nil, false)
					#menu_groupe false, false			
				else
					#menu_groupe true, false
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Groupes", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		@groupe2.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nb d'article", renderer_left, :text => 2)
		col.resizable = true
		@groupe2.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	
  	scroll.add_with_viewport @groupe2
  	
  	scroll
	
	end
	
	def ged
		
		conf = @window.config_db.conf
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
    	
    	table = Gtk::Table.new 10, 2, false
		
		i = 0
		# Le chemin vers les documents
		label_chemin_documents = Gtk::Alignment.new 0, 0.5, 0, 0
		label_chemin_documents.add Gtk::Label.new("Chemin vers les documents:")
		@chemin_documents = Gtk::Entry.new
		@chemin_documents.text = conf["chemin_documents"].to_s unless conf.nil?
		table.attach( label_chemin_documents, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @chemin_documents, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		filechooserbt = Gtk::Button.new
		filechooserbt.image = Gtk::Image.new( "./resources/icons/add.png" )
		filechooserbt.signal_connect("clicked"){ }
		table.attach( filechooserbt, 2, 3, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )
		
		
		i += 1
		# Le scanner
		label_scanner = Gtk::Alignment.new 0, 0.5, 0, 0
		label_scanner.add Gtk::Label.new("Adresse du scanner:")
		@scanner = Gtk::Entry.new
		@scanner.text = conf["scanner"].to_s unless conf.nil?
		table.attach( label_scanner, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @scanner, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		i += 1
		# La résolution du scanner
		label_resolution = Gtk::Alignment.new 0, 0.5, 0, 0
		label_resolution.add Gtk::Label.new("Résolution du scanner (en dpi):")
		@resolution = Gtk::Entry.new
		@resolution.text = conf["resolution"].to_s unless conf.nil?
		table.attach( label_resolution, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 2, 2 )	
		table.attach( @resolution, 1, 2, i, i+1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 2, 2 )
		
		scroll.add_with_viewport table
    	
    scroll
	
	end
	
	def refresh
	
		refresh_societe
		#refresh_ged
		refresh_groupe true
		#refresh_groupe false
		@groupe.expand_all
	
	end
	
	def remplir_language	
		@language.model.clear
		languages = [ {:code => 'fr', :lang => 'Français' },
									{:code => 'en', :lang => 'English' },
									{:code => 'rcf', :lang => 'Kréol réioné' }
								]
		
		active_iter = nil
		languages.each do |lang|
			iter = @language.model.append
			iter[0] = lang[:code]
			iter[1] = lang[:lang]
			if @window.config_db.fichier["translation"]
				active_iter = iter if @window.config_db.fichier["translation"].eql?(lang[:code])		
			end						
		end
		@language.active_iter = active_iter if active_iter
	end
	
	def refresh_societe
		@societe = Societe.first
		@soc_nom.text = @societe.nom.to_s
		@soc_adresse.buffer.text = @societe.adresse.to_s
		@soc_capital.text = @societe.capital.to_s
		@soc_tel.text = @societe.tel.to_s
		@soc_fax.text = @societe.fax.to_s
		@soc_mail.text = @societe.mail.to_s
		@soc_site.text = @societe.site.to_s
		@soc_siret.text = @societe.siret.to_s
		@soc_rcs.text = @societe.rcs.to_s
		@soc_naf.text = @societe.naf.to_s
		@soc_status.text = @societe.status.to_s
		@soc_capital.text = @societe.capital.to_s
		societe = Societe.first
		logo = "#{@window.config_db.conf["chemin_documents"]}/societe/#{societe.logo}"
		@soc_logo.file = logo if File.exist?(logo)
		remplir_language
	end
	
	def refresh_groupe culture=true
		
		if culture 
			@groupe.model.clear
		else
			@groupe2.model.clear
		end
		
		groupesp = (culture ? Culturegroupe.where(:parent_id => 0).order(:designation) : Articlegroupe.where(:parent_id => 0).order(:designation) )
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if culture
					iter = @groupe.model.append nil
				else
					iter = @groupe2.model.append nil
				end
				iter[0] = gr.id
				iter[1] = gr.designation	
				cultures = (culture ? Culture.where(:culturegroupe_id => gr.id)	: Article.where(:articlegroupe_id => gr.id) )
				iter[2] = cultures.count
				groupese = (culture ? Culturegroupe.where("parent_id=? AND id<>?", gr.id, 0).order(:designation) : Articlegroupe.where("parent_id=? AND id<>?", gr.id, 0).order(:designation) )						
				if groupese then
					groupese.each do |gr2|
						iter2 = (culture ? @groupe.model.append(iter) : @groupe2.model.append(iter))
						iter2[0] = gr2['id'].to_i
						iter2[1] = gr2['designation']
						cultures = (culture ? Culture.where(:culturegroupe_id => gr2.id) : Article.where(:articlegroupe_id => gr2.id))
						iter2[2] = cultures.count
					end
				else
					@window.message_erreur groupese.error
				end
			end
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def refresh_ged
		@chemin_documents.text = @window.config_db.conf["chemin_documents"].to_s
		@scanner.text = @window.config_db.conf["scanner"].to_s
		@resolution.text = @window.config_db.conf["resolution"].to_s
	end
	
	def menu_groupe vide=false, culture=true
	
		add_tb = Gtk::MenuItem.new "Ajouter un groupe"
		add_tb.signal_connect( "activate" ) { 
			dial_add = DialGroupe.new @window, nil, culture
			dial_add.run { |response| 
				if response.eql?(-5)
					if !dial_add.designation.text.empty?
						designation = dial_add.designation.text
						parent_id = dial_add.parent.model.get_value(dial_add.parent.active_iter,0)
						couleur = dial_add.couleur.text
						if culture 
							Culturegroupe.create :designation => designation, :parent_id => parent_id, :couleur => couleur
						else
							Articlegroupe.create :designation => designation, :parent_id => parent_id, :couleur => couleur
						end
						refresh_groupe culture
						#@window.liste_cultures.remplir_groupe
					end
				end
				dial_add.destroy 			
			}		
		}
		
		menu = Gtk::Menu.new
		menu.append add_tb
		
		if !vide
			modifier_tb = Gtk::MenuItem.new "Modifier le groupe"
			modifier_tb.signal_connect( "activate" ) { 
				id = @groupe.model.get_value( @groupe.selection.selected, 0 )
				dial_add = DialGroupe.new @window, id, culture
				dial_add.run { |response| 
					if response.eql?(-5)
						if !dial_add.designation.text.empty?
							designation = dial_add.designation.text
							parent_id = dial_add.parent.model.get_value(dial_add.parent.active_iter,0)
							couleur = dial_add.couleur.text
							if culture 
								gr = Culturegroupe.find(id)
							else
								gr = Articlegroupe.find(id) 
							end 
							gr.designation = designation
							gr.parent_id = parent_id
							gr.couleur = couleur
							gr.save
							refresh_groupe culture
							#@window.liste_cultures.remplir_groupe
						end
					end
					dial_add.destroy 			
				}		
			}
		
			suppr_tb = Gtk::MenuItem.new "Supprimer le groupe"
			suppr_tb.signal_connect( "activate" ) { 
				suppr_groupe culture	
			}
		
			menu.append modifier_tb
			menu.append suppr_tb
		end
		
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def suppr_groupe culture=true
		suppr = false
			
		dialog = Gtk::MessageDialog.new(@window, 
	                        Gtk::Dialog::DESTROY_WITH_PARENT,
	                        Gtk::MessageDialog::QUESTION,
	                        Gtk::MessageDialog::BUTTONS_YES_NO,
	                        "Voulez-vous réellement supprimer ce groupe ?")
		response = dialog.run
		case response
		  when Gtk::Dialog::RESPONSE_YES
			suppr = true
		end 
		dialog.destroy
		
		if suppr
			id = (culture ? @groupe.model.get_value( @groupe.selection.selected, 0 ) : @groupe2.model.get_value( @groupe2.selection.selected, 0 ))
			cultures = (culture ? Culture.where(:culturegroupe_id => id) : Article.where(:articlegroupe_id => id))
			if cultures.count.eql?(0)
				gr = (culture ? Culturegroupe.where(:parent_id => id) : Articlegroupe.where(:parent_id => id))
				if gr.count.eql?(0)
					if culture
						Culturegroupe.delete(id) 
					else
						Articlegroupe.delete(id)
					end
					refresh_groupe culture
					#@window.liste_cultures.remplir_groupe
				else
					@window.message_erreur "Ce groupe ne peut pas être supprimé car au moins un sous-groupe y est rattaché."
				end
			else
				if culture 
					@window.message_erreur "Ce groupe ne peut pas être supprimé car au moins une culture y est rattachée."
				else
					@window.message_erreur "Ce groupe ne peut pas être supprimé car au moins un article y est rattaché."
				end
			end
		end
	end
	
	def validate quitter=true
		
		res = false
		if !@soc_nom.text.empty? then
			
			res = save_societe
			
			#save_ged if res
			
			if res then
				quit if quitter
			else
				@window.message_erreur res.error
			end
		else
			@window.message_erreur "Vous devez saisir au moins un nom pour votre société !"
			res = false
		end
		return res
	end
	
	def save_societe
		
		@societe = Societe.first
		@societe.nom = @soc_nom.text
		@societe.adresse = @soc_adresse.buffer.text
		@societe.capital = @soc_capital.text
		@societe.tel = @soc_tel.text
		@societe.fax = @soc_fax.text
		@societe.mail = @soc_mail.text
		@societe.site = @soc_site.text
		@societe.siret = @soc_siret.text
		@societe.rcs = @soc_rcs.text
		@societe.naf = @soc_naf.text
		@societe.status = @soc_status.text
		@societe.capital = @soc_capital.text.to_i
		
		lang = @language.model.get_value(@language.active_iter,0).to_s
		if @window.config_db.fichier["translation"]!=lang
			@window.config_db.fichier["translation"] = @language.model.get_value(@language.active_iter,0).to_s
			@window.config_db.save_config
			MessageController.message @window, "INFO", "Vous devez redémarrer AgriTux pour que le changement de langue soit pris en compte."
		end
		
		res = @societe.save
		
		res
	end
	
	def save_ged
		@window.config_db.conf["chemin_documents"] = @chemin_documents.text
		@window.config_db.conf["scanner"] = @scanner.text
		@window.config_db.conf["resolution"] = @resolution.text
		File.open(@chemin_config, 'w') do |out|
   			YAML.dump(@window.config_db.conf, out)
		end
	end
	
	def naviguer
		
		if !@soc_site.text.include?("http://")
			@soc_site.text = "http://" + @soc_site.text
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open #{@soc_site.text}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def nouveau_logo
		dialog = Gtk::FileChooserDialog.new("Open File",
					                         @window,
					                         Gtk::FileChooser::ACTION_OPEN,
					                         nil,
					                         [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
					                         [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		filtre = Gtk::FileFilter.new
		filtre.add_pattern("*.png")
		dialog.filter = filtre
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			fichier = dialog.filename
			chemin_dossier = "#{@window.config_db.conf["chemin_documents"]}/societe/"
			chemin = "#{chemin_dossier}#{File.basename(fichier)}"
			# on créer les dossiers si ils n'existent pas déjà
			Dir.mkdir(chemin_dossier) unless File.directory?(chemin_dossier)
			# on fait la copie de fichier
			FileUtils.cp fichier, chemin
			# Puis on créé une entrée en base de donnée pour la ged
			@soc_logo.file = chemin
			# Enfin on enregistre le choix dans la base de données
			societe = Societe.first
			societe.logo = File.basename(fichier)
			societe.save
		end
		dialog.destroy
	end
	
	def quit
	
		@window.liste_cultures.refresh
  	@window.affiche @window.liste_cultures
	
	end
	
end
