# coding: utf-8

class ListeTiers_box < Gtk::VBox

	attr_reader :view, :search, :filtre, :add_new
	
	def initialize window, research = false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@research = research
		
		agencement
		
		#refresh 2
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/icons/add.png" )
		@label_button = Gtk::Label.new "Nouveau tiers"
		hboxajout.add @label_button
		@add_new.add hboxajout
		@compteur = Gtk::Label.new
		
		hbox = Gtk::HBox.new false, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		hbox.add @search
		
		@filtre = Gtk::ComboBox.new
		remplir_filtre
		@filtre.active = 2
		hbox.pack_start Gtk::Label.new("Filtres :"), false, false, 3 unless @research
		hbox.pack_start @filtre, false, false, 3 unless @research
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add add_new
		
		hbox.add align_button
		
		@frame = Gtk::Frame.new "Tiers"
		vbox.pack_start hbox, false, false, 3
		vbox.pack_start tableau_documents, true, true, 3 
		vbox.pack_start @compteur, false, false, 3
		@frame.add vbox
		
		if !@research
			@add_new.signal_connect( "clicked" ) {
				@window.tiers.refresh
				@window.affiche @window.tiers
			}
		end
		
		@filtre.signal_connect('changed') {
			changement_filtre
		}
		
		@search.signal_connect('activate') {
			if !@search.text.empty? then
				@filtre.active = -1
				refresh @filtre.active, @search.text
				#@search.text = ""
			else
				#refresh @filtre.active
				@filtre.active = 2
			end
		}
		
		add @frame
	
	end
	
	def tableau_documents
	
		# list_store (id, nom, type, tarif, adresse, cp, ville, tel, id_tarif, mail)
		@list_store = Gtk::ListStore.new(Integer, String, String, String, String, String, String, String, Integer, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				 @window.tiers.refresh @view.model.get_value(@view.selection.selected, 0)
				 @window.affiche @window.tiers
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Nom", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Type de tiers", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tél.", renderer_left, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Adresse", renderer_left, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Code postal", renderer_right, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Ville", renderer_left, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def refresh filtre=2, term=nil
		
		term = @search.text unless @search.text.empty?
		tiers = Tiers.includes(:typetiers).order("nom")
		if term
			t = Tiers.arel_table
			tiers = tiers.where(t[:nom].matches("%#{term}%").or(t[:tel].matches("%#{term}%")))
		end
		tiers = tiers.where("typetiers_id=? OR typetiers_id=?", 2, 4) if filtre.eql?(0)
		tiers = tiers.where("typetiers_id=? OR typetiers_id=?", 3, 4) if filtre.eql?(1)
		
		remplir tiers
		
		@compteur.text = "Nombre d'éléments dans la liste : #{tiers.count}"

	end
	
	def remplir tiers

		@list_store.clear
		
		tiers.each { |h| 
			
			iter = @list_store.append
			iter[0] = h.id.to_i
			iter[1] = h.nom
			iter[2] = h.typetiers.designation
			iter[4] = h.adresse
			iter[5] = h.cp
			iter[6] = h.ville
			iter[7] = h.tel
			iter[9] = h.mail
		}
			
	end
	
	def remplir_filtre
	
		@filtre.model.clear
		@filtre.append_text "Clients" #0
		@filtre.append_text "Fournisseurs" #1
		@filtre.append_text "Tous" #2
 	  
	end
	
	def changement_filtre
		@search.text = "" unless @filtre.active<0
		refresh @filtre.active unless @filtre.active<0
	end
	
	def focus
		@search.grab_focus
	end
	
end
