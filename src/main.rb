# coding: utf-8

class MainWindow < Gtk::Window
	
		attr_reader :tableau_bord, :type_doc, :login, :culture, :tiers, :parcelle, :article, :subvention
		attr_reader :liste_users, :liste_cultures, :liste_tiers, :liste_parcelles, :liste_articles, :liste_subventions
		attr_reader :version, :utilisateur, :scan
		attr_reader :configuration, :liste_interventions, :unites
		attr_accessor :config_db
	
	def initialize level
	
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }
		
		@version = "0.4.5"
		set_title GLib.application_name + " v" + @version
		
		set_window_position Gtk::Window::POS_CENTER
		set_default_size 800, 300
		set_icon( "./resources/icons/agritux128.png" )	
		
		@unites = ["unité", "g", "kg", "t", "m", "m2", "m3", "heures", "jours", "L", "Forfait"]	
		
		@config_db = ConfigDb.new self
		
		lancement		
	
	end
	
	def lancement
		
		@toolbargen = ToolBarGen_box.new self
		
		@vbox = Gtk::VBox.new false, 2
		@login = Login_box.new self
		@vbox.pack_start @login, false, false, 2
		
		add @vbox
		
		show_all
		
		@login.refresh
	end
	
	def demarre_appli
	
		@toolbargen.each { |child|
			child.sensitive = true
		}
		
		# If not a admin, config menu is hidden
		@toolbargen.children[8].sensitive = false unless @login.user.admin
		
		@tableau_bord = TableauBord_box.new self
		@culture = Culture_box.new self
		@parcelle = Parcelle_box.new self
		@article = Article_box.new self
		@subvention = Subvention_box.new self
		@tiers = Tiers_box.new self
		@liste_parcelles = ListeParcelles_box.new self
		@liste_cultures = ListeCultures_box.new self
		@liste_articles = ListeArticles_box.new self
		@liste_subventions = ListeSubventions_box.new self
		@liste_users = ListeUsers_box.new self
		@liste_tiers = ListeTiers_box.new self
		@utilisateur = Utilisateur_box.new self
		@scan = Scan.new @config_db
		@configuration = Configuration_box.new self
		
		societe = Societe.first
		set_title "#{GLib.application_name} v #{@version} - #{societe.nom}"
		
		@vbox.remove @login
		@vbox.pack_start @toolbargen, false, false, 0
		@vbox.pack_start @tableau_bord, true
    self.show_all
		@tableau_bord.refresh
	
	end
	
	def deconnecter
	
		if MessageController.question_oui_non self, t.global.confirm_logout
			@vbox.children.each do |c|
				@vbox.remove c
			end
	
			@vbox.pack_start @login, false, false, 2
			@login.refresh
	
			set_title "#{GLib.application_name} v #{@version}"
	
			self.unmaximize
		end 
	
	end
	
	def efface_vbox_actuelle
	
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	
	end
	
	def affiche vbox
	
		efface_vbox_actuelle
		@vbox.pack_start vbox, true
    self.show_all
	
	end	
	
	def quit
		
		dialog = Gtk::MessageDialog.new(self, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                Gtk::MessageDialog::QUESTION,
                                Gtk::MessageDialog::BUTTONS_YES_NO,
                                t.global.confirm_exit)
		response = dialog.run
		case response
		  when Gtk::Dialog::RESPONSE_YES
				Gtk.main_quit
		end 
		dialog.destroy
		
	end
	
	def message_erreur message
		MessageController.erreur self, message
	end
	
	def message_attention message
		MessageController.attention self, message
	end

end
