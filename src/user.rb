# coding: utf-8

class Utilisateur_box< Gtk::VBox

	def initialize window
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		@id_user = -1
		
		@firstname = Gtk::Entry.new
		@lastname = Gtk::Entry.new
		@username = Gtk::Entry.new
		@password = Gtk::Entry.new
		@password.visibility = false
		@admin = Gtk::CheckButton.new
		@active = Gtk::CheckButton.new
		@active.active = true

		@valider = Gtk::Button.new Gtk::Stock::OK
		@annuler = Gtk::Button.new Gtk::Stock::CANCEL
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { cancel }
	
	end
	
	def agencement
	
		hbox2 = Gtk::HBox.new true, 2
		
		vbox2 = Gtk::VBox.new false, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Prénom :" )
		vbox2.add @firstname
		hbox2.pack_start( align3, true, true, 3 )

		vbox2 = Gtk::VBox.new false, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Nom :" )
		vbox2.add @lastname
		hbox2.pack_start( align3, true, true, 3 )
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		vboxframe = Gtk::VBox.new false, 2
		vboxframe.pack_start hbox2, false, false, 3
		
		hboxusername = Gtk::HBox.new false, 2
		hboxusername.pack_start( Gtk::Label.new( "Nom d'utilisateur :" ), false, false, 3 )
		hboxusername.pack_start( @username, false, false, 3 )
		
		vboxframe.pack_start hboxusername, false, false, 3
		
		hboxmdp = Gtk::HBox.new false, 2
		hboxmdp.pack_start( Gtk::Label.new( "Mot de passe :" ), false, false, 3 )
		hboxmdp.pack_start( @password, false, false, 3 )
		
		vboxframe.pack_start hboxmdp, false, false, 3
		
		hboxadmin = Gtk::HBox.new false, 2
		hboxadmin.pack_start( Gtk::Label.new( "Administrateur ? " ), false, false, 3 )
		hboxadmin.pack_start( @admin, false, false, 3 )
		
		vboxframe.pack_start hboxadmin, false, false, 3
		
		hboxactif = Gtk::HBox.new false, 2
		hboxactif.pack_start( Gtk::Label.new( "Actif ? " ), false, false, 3 )
		hboxactif.pack_start( @active, false, false, 3 )
		
		vboxframe.pack_start hboxactif, false, false, 3
		
		@frame = Gtk::Frame.new "Nouvel utilisateur"
		@frame.add vboxframe
		
		pack_start @frame, true, true, 3
		pack_start align1, false, false, 3
	
	end
	
	def validate
		
		if ( !@firstname.text.empty? and !@lastname.text.empty? and !@username.text.empty? and ( !@password.text.empty? or @id_user>0 ) ) then
			
			@utilisateur.id = @id_user if @id_user>0
			@utilisateur.firstname = @firstname.text
			@utilisateur.lastname = @lastname.text
			@utilisateur.username = @username.text
			if (@id_user>0 and @password.text.empty?)
				# on ne touche pas au mot de passe déjà enregistré
			else
				@utilisateur.password = BCrypt::Password.create(@password.text)
			end
			@utilisateur.admin = @admin.active?
			@utilisateur.active = @active.active?
			
			res = @utilisateur.save
			
			quit 1 if res
			
		else
			erreur = ""
			erreur += "Le prénom n'est pas renseigné.\n" if @firstname.text.empty?
			erreur += "Le nom n'est pas renseigné.\n" if @lastname.text.empty?
			erreur += "Le nom d'utilisateur n'est pas renseigné.\n" if @username.text.empty?
			erreur += "Le mot de passe n'est pas renseigné.\n" if ( @password.text.empty? and @id_user<0 )
			dialog = Gtk::MessageDialog.new @window, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
    		dialog.run
		end
	
	end
	
	def cancel
	
		quit
	
	end
	
	def quit statut=nil
	
		@id_user = -1
		@firstname.text = ""
		@lastname.text = ""
		@username.text = ""
		@admin.active = false
		@active.active = true
		
		@window.liste_users.remplir_users unless statut.nil?
		@window.affiche @window.liste_users
	
	end
	
	def change id=0
		@id_user = id
		@utilisateur = nil
		if @id_user>0 then
			
			@utilisateur = Utilisateur.find(@id_user)		
			if @utilisateur then
				@firstname.text = @utilisateur.firstname
				@lastname.text = @utilisateur.lastname
				@username.text = @utilisateur.username
				@admin.active = @utilisateur.admin
				@active.active = @utilisateur.active
			else
				@window.message_erreur utilisateur.error
			end
		else
			p "nouvel utilisateur"
			@utilisateur = Utilisateur.new
		end
		
		@frame.label = (@id_user>0 ? "Modifier utilisateur" : "Nouvel utilisateur")
	end
	
end
