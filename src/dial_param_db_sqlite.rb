# coding: utf-8

class DialParamDbSqlite < Gtk::Dialog

	attr_reader :nom, :database, :chemin_documents
	
	def initialize window, id
		
		super("Paramètres de la base de données Sqlite", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 500, 200
		
		@nom = Gtk::Entry.new
		@nom.text = window.config_db.bases[id]["nom"].to_s if id>-1
		@database = Gtk::Entry.new
		@database.text = window.config_db.bases[id]["database"].to_s if id>-1
		@database.sensitive = id<0
		@chemin_documents = Gtk::Entry.new
		@chemin_documents.text = window.config_db.bases[id]["chemin_documents"].to_s if id>-1
		
		table = Gtk::Table.new 3, 3, false
		
		i=0
		table.attach Gtk::Label.new( "Nom de la connexion" ), 0, 1, i, i+1
		table.attach @nom, 1, 2, i, i+1
		
		i+=1
		table.attach Gtk::Label.new( "Nom du fichier de base de données" ), 0, 1, i, i+1
		table.attach @database, 1, 2, i, i+1
		
		#i+=1
		#table.attach Gtk::Label.new( "Chemin vers les documents" ), 0, 1, i, i+1
		#table.attach @chemin_documents, 1, 2, i, i+1
  	
  	frame = Gtk::Frame.new
		frame.label = (id<0 ? "Nouvelle base de données Sqlite" : "Modification de la base de données Sqlite")
		frame.add table
		
		self.vbox.pack_start frame, true, true, 3
		
		self.vbox.show_all

	end

end
