# coding: utf-8

class Culture_box < Gtk::VBox

	attr_reader :culture, :historique
	MAX_SPIN = 999999

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@image_chargement = Gdk::Pixbuf.new "resources/icons/chargement.png"
		@image_nograph = Gdk::Pixbuf.new "resources/icons/nograph.png"
		@activer_combo = false
		
		@info1 = Gtk::Entry.new
		@info2 = Gtk::Entry.new
		@description = Gtk::TextView.new
		@datecreation = Gtk::Calendar.new
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new t.global.validate
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new t.global.cancel
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::HBox.new false, 2
		hboxsupprimer.add Gtk::Image.new( "./resources/icons/trash-full.png" )
		hboxsupprimer.add Gtk::Label.new t.global.delete
		@supprimer.add hboxsupprimer
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = t.farming.new
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start @supprimer, false, false, 3
		hbox.pack_start align1, true, true, 3
		
		vbox.pack_start( hbox, false, false, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		vbox = Gtk::VBox.new false, 3
		table = Gtk::Table.new 3, 9, false
		@code = Gtk::Entry.new
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new @groupe_model
		renderer_left = Gtk::CellRendererText.new
		
		@designation = Gtk::Entry.new		
		@description = Gtk::TextView.new	
		@image = Gtk::Image.new
		scroll_desc = Gtk::ScrolledWindow.new
		
		# Propriétés
		@description.wrap_mode = Gtk::TextTag::WRAP_WORD
		@code.width_chars = 8
		renderer_left.xalign = 0
		scroll_desc.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_desc.shadow_type = Gtk::SHADOW_IN
		
		# Agencement				
		table.attach( Gtk::Label.new(t.global.code), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @code, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		@groupe.pack_start renderer_left, true
		@groupe.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new(t.global.group), 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @groupe, 3, 4, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( @image, 4, 8, 0, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.designation), 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @designation, 1, 4, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )		
		
		vbox.pack_start table, false, false, 3
		
		hbox = Gtk::HBox.new true, 23
		vbox_note = Gtk::VBox.new false, 3
		vbox_cycle = Gtk::VBox.new false, 3
		hbox.add vbox_cycle
		hbox.add vbox_note
		
		vbox_cycle.pack_start cycles, true, true, 3
		
		align_desc = Gtk::Alignment.new 0, 0, 0, 0
		align_desc.add Gtk::Label.new( t.global.description )
		
		scroll_desc.add @description
		vbox_note.pack_start align_desc, false, false, 3
		vbox_note.pack_start scroll_desc, true, true, 3
		
		vbox_note.pack_start notebook, true, true, 3
		
		vbox.pack_start hbox, true, true, 3
		
		# Renvoi
		vbox
	
	end
	
	def cycles
	
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_courant = Gtk::ScrolledWindow.new
		scroll_historique = Gtk::ScrolledWindow.new
		@cycle = Cycle_box.new @window, false
		@historique = Cycle_box.new @window, true
		
		# Propriétés des objets
		scroll_courant.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_historique.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		
		# Agencement du notebook
		scroll_courant.add_with_viewport @cycle
		scroll_historique.add_with_viewport @historique
		notebook.append_page scroll_courant, Gtk::Label.new(t.farming.current_cycle)
		notebook.append_page scroll_historique, Gtk::Label.new(t.farming.historical)
	
		# Renvoie
		notebook
	end
	
	def notebook
	
		# Objets	
		@note = Gtk::TextView.new
		renderer_left = Gtk::CellRendererText.new
		@ged = Ged_box.new @window, "cultures"
		@graph = Gtk::Image.new
		@annee = Gtk::ComboBox.new
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new
		table_carac = Gtk::Table.new 2, 4, false
		scroll_carac = Gtk::ScrolledWindow.new
		scroll_stat = Gtk::ScrolledWindow.new
		hbox_stat = Gtk::HBox.new false, 2
		vbox_stat = Gtk::VBox.new false, 2
		
		#Unité de vente
		label_unite = Gtk::Alignment.new 0, 0.5, 0, 0
		label_unite.add Gtk::Label.new(t.farming.sell_unit)
		table_carac.attach( label_unite, 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		@unite = Gtk::ComboBox.new
		table_carac.attach( @unite, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		#Prix
		label_prix = Gtk::Alignment.new 0, 0.5, 0, 0
		label_prix.add Gtk::Label.new(t.farming.sell_price)
		table_carac.attach( label_prix, 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		@prix = Gtk::Entry.new 
		table_carac.attach( @prix, 1, 2, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		#Subventions
		label_subventions = Gtk::Alignment.new 0, 0.5, 0, 0
		label_subventions.add Gtk::Label.new(t.global.grants)
		table_carac.attach( label_subventions, 0, 1, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )
		@subventions = Gtk::Entry.new
		table_carac.attach( @subventions, 1, 2, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )

		# Propriétés des objets
		@note.wrap_mode = Gtk::TextTag::WRAP_WORD
		scroll_note.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_note.shadow_type = Gtk::SHADOW_NONE
		scroll_carac.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_carac.shadow_type = Gtk::SHADOW_NONE
		scroll_stat.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		@annee.active = 0
		@annee.signal_connect('changed') {
			graphique if @activer_combo
		}
		
		# Agencement du conteneur Caractéristique
		scroll_carac.add_with_viewport table_carac
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du conteneur Stat
		vbox_stat.pack_start hbox_stat, false, false, 3
		vbox_stat.pack_start scroll_stat, true, true, 3
		hbox_stat.pack_start Gtk::Label.new(t.global.year), false, false, 3
		hbox_stat.pack_start @annee, false, false, 3
		scroll_stat.add_with_viewport @graph
		
		# Agencement du notebook
		#notebook.append_page @ged, Gtk::Label.new("GED")
		notebook.append_page scroll_carac, Gtk::Label.new(t.farming.features)
		notebook.append_page scroll_note, Gtk::Label.new(t.global.notes)
		#notebook.append_page vbox_stat, Gtk::Label.new("Stats")

		# Renvoie
		notebook
		
	end
	
	def refresh id=0
	
		@id = id
		@code.text = ""
		@designation.text = ""
		@note.buffer.text = ""
		@culture = nil
		
		if id>0 then
			@frame.label = "Culture n° " + id.to_s
			im = nil
			format = ["jpg", "JPG", "jpeg", "JPEG", "gif", "GIF", "png", "PNG"]
			format.each do |f|
				if File.exist?("./resources/cultures/#{@id}.#{f}")
					im = "./resources/cultures/#{@id}.#{f}"
				end
			end
		
			if im
				@image.pixbuf = Gdk::Pixbuf.new(im, width = 150, height = 150)
			else
				@image.clear
			end
			@culture = Culture.find(id)
			if @culture then
				@code.text = @culture.code
				@code.sensitive = false
				@designation.text = @culture.designation
				@note.buffer.text = @culture.note.to_s
				@description.buffer.text = @culture.description.to_s
				remplir_groupe @culture.culturegroupe_id.to_i
				@prix.text = "%.2f" % @culture.prix
				@subventions.text = "%.2f" % @culture.subventions.to_f
			else
				@window.message_erreur @culture.error
			end
			
			@ged.refresh @id
			@cycle.refresh @id
			@historique.refresh @id
			
			remplir_unite @culture.unite
			@supprimer.sensitive = true
			
		else
			@frame.label = t.farming.new
			@culture = Culture.new
			@image.clear
			@code.sensitive = true
			remplir_groupe
			@description.buffer.text = ""
			@prix.text = "0.00"
			@subventions.text = "0.00"
			remplir_unite
			@supprimer.sensitive = false
			
			@graph.clear
			
		end
	
	end
	
	def remplir_unite x=""
		@window.unites.count.times do
			@unite.remove_text 0
		end
		unit = @window.unites
		i=0
		unit.each do |u|
			@unite.append_text u
			if x.eql?(u)
				@unite.active = i
			end
			i += 1
		end
		@unite.active = 0 if @unite.active.eql?(-1)
	end
	
	def graphique
	
		@graph.pixbuf = @image_chargement
		combo_sensitive false
		
		Thread.new {
			x = (@window.size[0]/2).ceil
			x -= 50
			x = (x>400 ? x : 400)
			y = (@window.size[1]/2).ceil
			y -= 185
			y = (y>250 ? y : 250)
			
			g = Gruff::Bar.new "#{x}x#{y}"
			
			g.theme_pastel
		
			g.theme = {
				:background_colors => 'transparent'
			}
		
			#graph_donnees.each do |d|
			#	g.data(d[:titre], d[:serie], d[:color])
			#end
		
			g.labels = {0=>"1",1=>"2",2=>"3",3=>"4",4=>"5",5=>"6",6=>"7",7=>"8",8=>"9",9=>"10",10=>"11",11=>"12"}
		
			g.title = "Quantité vendue et achetée pour l'année #{@annee.active_text}"
		
			g.legend_font_size = g.legend_box_size = g.marker_font_size = 8
			g.title_font_size = 12
		
			chemin = "#{@window.config_db.chemin_temp}/graph.png"
			g.write chemin
			@graph.file = chemin if File.exist?(chemin)
			
			@activer_combo = true
			combo_sensitive true
		}
	
	end
	
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		groupesp = Culturegroupe.order(:designation)
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if gr.parent_id.eql?(0)
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end						
					groupesp.each do |gr2|
						if gr2.parent_id.eql?(gr.id)
							iter2 = @groupe_model.append iter
							iter2[0] = gr2.id.to_i
							iter2[1] = gr2.designation
					
							if !nom.nil?
								active_iter = iter2 if nom.eql?(gr2.id)		
							end	
						end
					end
				end
			end
		
			if !active_iter.nil? then @groupe.active_iter = active_iter end
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		if !(@id>0) then
			if @code.text.empty? then
				error += t.farming.no_code
			else
				error += t.farming.code_exist if Culture.where(:code => @code.text).count>0
			end
		end
		error += @designation.text.empty? ? t.farming.designation_empty : ""
		
		# Si pas d'erreurs on continue
		if error.empty? then

			groupe = @groupe_model.get_value(@groupe.active_iter,0).nil? ? 0 : @groupe_model.get_value(@groupe.active_iter,0)
		
			# sauvegarde des données de l'article
			@culture.designation = @designation.text
			@culture.code = @code.text
			@culture.description = @description.buffer.text
			@culture.note = @note.buffer.text
			@culture.culturegroupe_id = groupe
			@culture.prix = @prix.text.to_f
			@culture.subventions = @subventions.text.to_f
			@culture.unite = @unite.active_text

			res = @culture.save
		
			quit if quitter
		
		else
			@window.message_erreur error
		end
		return res
	
	end
	
	def supprimer
	
		if @id>0
			suppr = false
			dialog = Gtk::MessageDialog.new(@window, 
					                        Gtk::Dialog::DESTROY_WITH_PARENT,
					                        Gtk::MessageDialog::QUESTION,
					                        Gtk::MessageDialog::BUTTONS_YES_NO,
					                        t.farming.confirm_delete)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
				suppr = true
			end 
			dialog.destroy
		
			if suppr
		
				lignes = Cultureligne.where(:culture_id => @id)
				if lignes.count > 0
					@window.message_erreur t.farming.error_delete
				else
					@culture.destroy
					quit
				end
						
			end
		end
	
	end
	
	def combo_sensitive status
		@annee.sensitive = status
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def quit
	
		@window.liste_cultures.refresh
		@window.affiche @window.liste_cultures
		@window.liste_cultures.focus
	
	end
	
end

