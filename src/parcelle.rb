# coding: utf-8

class Parcelle_box < Gtk::VBox

	attr_reader :parcelle
	MAX_SPIN = 999999

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new t.global.validate
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new t.global.cancel
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::HBox.new false, 2
		hboxsupprimer.add Gtk::Image.new( "./resources/icons/trash-full.png" )
		hboxsupprimer.add Gtk::Label.new t.global.delete
		@supprimer.add hboxsupprimer
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = t.parcel.new
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, false, false, 3 )	
		vbox_corps.pack_start( notebook, true, true, 3 )	
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start @supprimer, false, false, 3
		hbox.pack_start align1, true, true, 3
		
		vbox.pack_start( hbox, false, false, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@code = Gtk::Entry.new
		renderer_left = Gtk::CellRendererText.new
		@taille = Gtk::Entry.new
		@designation = Gtk::Entry.new		
		@image = Gtk::Image.new(Gdk::Pixbuf.new("./resources/icons/parcelle.png", width = 150, height = 150))
		
		# Propriétés
		@code.width_chars = 8
		renderer_left.xalign = 0
		
		# Agencement		
		vbox = Gtk::VBox.new false, 3
		
		table = Gtk::Table.new 3, 9, false
		
		table.attach( Gtk::Label.new(t.global.code), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @code, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.parcel.size), 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @taille, 3, 4, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( @image, 4, 8, 0, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.designation), 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @designation, 1, 4, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )		
		
		vbox.pack_start table, false, false, 3
		
		align_desc = Gtk::Alignment.new 0, 0, 0, 0
		align_desc.add Gtk::Label.new( t.global.notes )
		
		vbox
	
	end
	
	def notebook
	
		# Objets	
		@note = Gtk::TextView.new
		renderer_left = Gtk::CellRendererText.new
		@cahier = Cahier_box.new(@window)
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new

		# Propriétés des objets
		@note.wrap_mode = Gtk::TextTag::WRAP_WORD
		scroll_note.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_note.shadow_type = Gtk::SHADOW_NONE
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du notebook
		notebook.append_page @cahier, Gtk::Label.new(t.dashboard.farming_book)
		notebook.append_page scroll_note, Gtk::Label.new(t.global.notes)

		# Renvoie
		notebook
		
	end
	
	def refresh id=0
	
		@id = id
		@code.text = ""
		@designation.text = ""
		@note.buffer.text = ""
		@parcelle = nil
		
		if id>0 then
			@frame.label = "#{t.global.parcel} n° " + id.to_s
			@parcelle = Parcelle.find(id)
			if @parcelle then
				@code.text = @parcelle.code
				@code.sensitive = false
				@designation.text = @parcelle.designation
				@note.buffer.text = @parcelle.note.to_s
				@taille.text = @parcelle.taille.to_s
			else
				@window.message_erreur @parcelle.error
			end
			
		else
			@frame.label = t.parcel.new
			@parcelle = Parcelle.new
			@image.clear
			@code.sensitive = true
			@note.buffer.text = ""
			@taille.text = ""
			
		end
		
		@cahier.refresh @id
		@supprimer.sensitive = id>0
	
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		if !(@id>0) then
			if @code.text.empty? then
				error += t.parcel.error_code
			else
				error += t.parcel.code_exist if Parcelle.where(:code => @code.text).count>0
			end
		end
		error += @designation.text.empty? ? t.parcel.designation_empty : ""
		
		# Si pas d'erreurs on continue
		if error.empty? then
		
			# sauvegarde des données de la parcelle
			@parcelle.designation = @designation.text
			@parcelle.code = @code.text
			@parcelle.note = @note.buffer.text
			@parcelle.taille = @taille.text.to_i

			res = @parcelle.save
		
			quit if quitter
		
		else
			@window.message_erreur error
		end
		return res
	
	end
	
	def supprimer
	
		if @id>0
			suppr = false
			dialog = Gtk::MessageDialog.new(@window, 
					                        Gtk::Dialog::DESTROY_WITH_PARENT,
					                        Gtk::MessageDialog::QUESTION,
					                        Gtk::MessageDialog::BUTTONS_YES_NO,
					                        t.parcel.delete)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
				suppr = true
			end 
			dialog.destroy
		
			if suppr
		
				lignes = Cultureligne.where(:parcelle_id => @id)
				if lignes.count > 0
					@window.message_erreur t.parcel.error_farming_exist
				else
					@parcelle.destroy
					quit
				end
						
			end
		end
	
	end
	
	def combo_sensitive status
		@annee.sensitive = status
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def quit
	
		@window.liste_parcelles.refresh
		@window.affiche @window.liste_parcelles
		@window.liste_parcelles.focus
	
	end
	
end

