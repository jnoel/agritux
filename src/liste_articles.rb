# coding: utf-8

class ListeArticles_box < Gtk::VBox

	attr_reader :view, :search_gencode, :add_new
	
	def initialize window, research = false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@research = research
		
		@position = [0,0]
		 
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new t.good.new
		@add_new.add hboxajout
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		# ComboBox pour le groupe articles
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new @groupe_model
		@groupe.pack_start renderer_left, true
		@groupe.add_attribute renderer_left, "text", 1
		remplir_groupe
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add @add_new
		frame = Gtk::Frame.new t.global.goods
		@compteur = Gtk::Label.new 
		
		hbox = Gtk::HBox.new false, 3
		@search_gencode = Gtk::Entry.new
		@search_gencode.primary_icon_stock = Gtk::Stock::FIND
		@search_gencode.secondary_icon_stock = Gtk::Stock::CLEAR
		
		# Agencement
		hbox.add @search_gencode
		
		hbox.pack_start Gtk::Label.new(t.global.group), false, false, 3 unless @research
		hbox.pack_start @groupe, false, false, 3 unless @research
		
		hbox.add align_button
		
		vbox.pack_start hbox, false, false, 3 
		vbox.pack_start tableau_articles, true, true, 3 
		vbox.pack_start @compteur, false, false, 3 
		@next = Gtk::Button.new ">"
		align_next = Gtk::Alignment.new 1, 1, 0, 0
		align_next.add @next
		frame.add vbox
		
		if !@research
			@add_new.signal_connect( "clicked" ) {
				@window.article.refresh
				@window.affiche @window.article
			}
		end
		
		@search_gencode.signal_connect('activate') {
			refresh @search_gencode.text
			@groupe.active = -1
		}
		
		@groupe.signal_connect('changed') {
			changement_groupe
		}
		
		add frame
	
	end
	
	def tableau_articles
	
		# list_store (id, code, designation, groupe, photo, unité, prix, prix_tri, stock, stock_tri)
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Gdk::Pixbuf, String, String, Float, String, Float)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				@window.article.refresh @view.model.get_value @view.selection.selected, 0
				@window.affiche @window.article
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_center = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		# Colonne pour la photo
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 4)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.code, renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.designation, renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.good.current_stock, renderer_right, :text => 8)
		col.sort_column_id = 9
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.unit, renderer_center, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.unit_price, renderer_right, :text => 6)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.group, renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh search=nil, groupe=nil
		
		groupe = nil if groupe==0
		
		search = @search_gencode.text unless @search_gencode.text.empty?
		
		articles = Article.includes(:articlegroupe).order("code")
		if search
			art = Article.arel_table
			articles = articles.where(art[:designation].matches("%#{search}%").or(art[:code].matches("%#{search}%")))
		end
		articles = articles.where("articlegroupe_id=? OR articlegroupes.parent_id=?", groupe, groupe) if groupe
		
		@compteur.text = "#{t.global.count_elements} : #{articles.count}"

		if articles then	
			remplir articles
		end	
	
	end
	
	def remplir res

		@list_store.clear
		
		res.each { |h| 	
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.code.to_s
			iter[2] = h.designation.to_s
			iter[3] = h.articlegroupe.designation.to_s unless h.articlegroupe.nil?
			
			im = nil
			format = ["jpg", "JPG", "jpeg", "JPEG", "gif", "GIF", "png", "PNG"]
			format.each do |f|
				if File.exist?("./resources/articles/#{h.articlegroupe_id}.#{f}")
					im = "./resources/articles/#{h.articlegroupe_id}.#{f}"
				end
			end
		
			if im
				pix = Gdk::Pixbuf.new(im, 48, 48)
				iter[4] = pix
			end
			
			iter[5] = h.unite.to_s
			iter[6] = "%.2f #{t.global.currency}" % h.prix.to_f
			iter[7] = h.prix.to_f
			iter[8] = "%.2f" % h.stock
			iter[9] = h.stock.to_f
		}
			
	end
	
	def focus
		@search_gencode.grab_focus
	end
	
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		groupesp = Articlegroupe.order(:designation)
		
		iter = @groupe_model.append nil
		iter[0] = 0
		iter[1] = t.global.all
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if gr.parent_id.eql?(0)
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end						
					groupesp.each do |gr2|
						if gr2.parent_id.eql?(gr.id)
							iter2 = @groupe_model.append iter
							iter2[0] = gr2.id.to_i
							iter2[1] = gr2.designation
					
							if !nom.nil?
								active_iter = iter2 if nom.eql?(gr2.id)		
							end	
						end
					end
				end
			end
		
			if !active_iter.nil? then @groupe.active_iter = active_iter end
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def changement_groupe
		groupe_id = (@groupe.model.get_value(@groupe.active_iter,0).nil? ? -1 : @groupe.model.get_value(@groupe.active_iter,0))
		refresh search=nil, groupe=groupe_id unless groupe_id.eql?(-1)
	end
	
end

