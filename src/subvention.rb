# coding: utf-8

class Subvention_box < Gtk::VBox

	attr_reader :subvention
	MAX_SPIN = 999999

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = t.grant.new
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, true, true, 3 )	
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new t.global.validate
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new t.global.cancel
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::HBox.new false, 2
		hboxsupprimer.add Gtk::Image.new( "./resources/icons/trash-full.png" )
		hboxsupprimer.add Gtk::Label.new t.global.delete
		@supprimer.add hboxsupprimer	
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start @supprimer, false, false, 3
		hbox.pack_start align1, true, true, 3
		
		vbox.pack_start( hbox, false, false, 3 )
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@designation = Gtk::Entry.new
		@raison = Gtk::TextView.new
		@date = Gtk::Calendar.new
		@montant = Gtk::Entry.new
		
		# Propriétés
		@raison.wrap_mode = Gtk::TextTag::WRAP_WORD
		
		# Agencement		
		vbox = Gtk::VBox.new false, 3
		
		table = Gtk::Table.new 2, 3, false
		
		table.attach( Gtk::Label.new(t.global.designation), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @designation, 1, 2, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.amount), 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @montant, 3, 4, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.reason), 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @raison, 0, 2, 2, 3, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.date), 2, 3, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @date, 2, 4, 2, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		vbox.pack_start table, true, true, 3
		
		vbox
	
	end
	
	def refresh id=0
	
		@id = id
		
		if id>0 then
			@frame.label = "#{t.global.grant} n° " + id.to_s
			@subvention = Subvention.find(id)
			if @subvention then
				@designation.text = @subvention.designation
				@raison.buffer.text = @subvention.raison.to_s
				@montant.text = "%.2f" % @subvention.montant
				@date.select_month(@subvention.date.month, @subvention.date.year)
				@date.select_day(@subvention.date.day)
			else
				@window.message_erreur @article.error
			end
			
		else
			@frame.label = t.grant.new
			@subvention = Subvention.new
			@designation.text = ""
			@raison.buffer.text = ""
			@montant.text = "0.0"
			@date.day = DateTime.now.day
			@date.month = DateTime.now.month
			@date.year = DateTime.now.year
		end
	
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		
		error = @designation.text.empty? ? t.grant.empty_desingation : ""
		
		# Si pas d'erreurs on continue
		if error.empty? then
		
			# sauvegarde des données de la subvention
			@subvention.designation = @designation.text
			@subvention.raison = @raison.buffer.text
			begin
				@subvention.date = Date.parse("#{@date.day}/#{@date.month+1}/#{@date.year}")
				@subvention.montant = @montant.text.to_f
				res = @subvention.save
				quit if quitter
			rescue
				@window.message_erreur "Format de date invalide : #{@date.day}/#{@date.month+1}/#{@date.year}"
			end
		
		else
			@window.message_erreur error
		end
		return res
	
	end
	
	def supprimer
	
		suppr = false
		if @id>0
			dialog = Gtk::MessageDialog.new(@window, 
					                        Gtk::Dialog::DESTROY_WITH_PARENT,
					                        Gtk::MessageDialog::QUESTION,
					                        Gtk::MessageDialog::BUTTONS_YES_NO,
					                        t.grant.confirm_delete)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
				suppr = true
			end 
			dialog.destroy
		end
		
		if suppr
			@subvention.destroy
			quit
		end
		
	end
	
	def focus
		@designation.grab_focus
	end
	
	def quit
	
		@window.liste_subventions.refresh
		@window.affiche @window.liste_subventions
		@window.liste_subventions.focus
	
	end
	
end

