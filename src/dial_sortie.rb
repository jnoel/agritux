# coding: utf-8

class DialSortie < Gtk::Dialog

	attr_reader :date, :quantite, :raison
	
	def initialize window
		
		super("Sortie d'articles", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		#set_default_size 900, 300
		
		table = Gtk::Table.new 3, 2, false
		
		#date
		label_date = Gtk::Alignment.new 0, 0.5, 0, 0
		label_date.add Gtk::Label.new("Date d'entrée:")
		table.attach( label_date, 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		@date = Gtk::Entry.new 
		table.attach( @date, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		#quantite
		label_quantite = Gtk::Alignment.new 0, 0.5, 0, 0
		label_quantite.add Gtk::Label.new("Quantite:")
		table.attach( label_quantite, 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		@quantite = Gtk::Entry.new
		table.attach( @quantite, 1, 2, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		#raison
		label_raison = Gtk::Alignment.new 0, 0.5, 0, 0
		label_raison.add Gtk::Label.new("Raison:")
		table.attach( label_raison, 0, 1, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )
		@raison = Gtk::Entry.new
		table.attach( @raison, 1, 2, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )
		
		self.vbox.pack_start table, true, true, 3
		
		self.vbox.show_all
	
	end

end		

