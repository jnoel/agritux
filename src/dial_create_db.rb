# coding: utf-8

class DialCreateDb < Gtk::Dialog
	
	def initialize window, connexion_id
		
		super("Migration de la base de données", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		self.set_response_sensitive Gtk::Dialog::RESPONSE_OK, false 
		set_default_size 500, 500
		
		@memo = Gtk::TextView.new
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @memo
  	
  	frame = Gtk::Frame.new
		frame.label = "Mise à jour de la base de données"
		frame.add scroll
		
		self.vbox.pack_start frame, true, true, 3
		
		self.vbox.show_all
		
		@memo.buffer.insert(@memo.buffer.start_iter,"=============================================\n")
		@memo.buffer.insert(@memo.buffer.end_iter,"Début de la migration... Veuillez patienter\n")
		@memo.buffer.insert(@memo.buffer.end_iter,"==============================================\n")
		
		migrate connexion_id

	end
	
	def migrate connexion_id
		Thread.new {
			if RUBY_PLATFORM.include?("mingw")
				@memo.buffer.insert(@memo.buffer.end_iter,`rake migrate[#{connexion_id}]`)
			else
				@memo.buffer.insert(@memo.buffer.end_iter,`bundle exec rake migrate[#{connexion_id}]`)
			end
			@memo.buffer.insert(@memo.buffer.end_iter,"\n=====================\n")
			@memo.buffer.insert(@memo.buffer.end_iter,"Fin de la migration\n")
			@memo.buffer.insert(@memo.buffer.end_iter,"=====================")

			self.set_response_sensitive Gtk::Dialog::RESPONSE_OK, true 
		}
	end

end
