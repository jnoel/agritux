#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'bundler/setup' unless RUBY_PLATFORM.include?("mingw")
require 'active_record'
require 'logger'
require 'gtk2'
require 'date'
require 'bcrypt'
require 'yaml'
require 'prawn'
#require 'prawn/layout'
require 'ezcrypto'
require 'r18n-desktop'

require '../db/models.rb'

Dir.foreach('.') { |f|
	require "./#{f}" if File.extname(f).eql?(".rb")
}

GLib.set_application_name "AgriTux"

include R18n::Helpers

Gtk.init
win = MainWindow.new :toplevel
Gtk.main
