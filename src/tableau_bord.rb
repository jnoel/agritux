# coding: utf-8

class TableauBord_box < Gtk::VBox
	
	def initialize window
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login	
		@rapport = RapportExercices.new window
		
		@gr = GraphBar.new
		@couleur_graph = [[1,0.65,0.65],[0.65,1,0.65],[0.596,0.855,0.957]]
		@unite = ["",t.global.currency]
		
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new true, 3
		@frame = Gtk::Frame.new t.dashboard.dashboard
				
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
		scroll.add_with_viewport @gr
		
		box_f = Gtk::HBox.new false, 3
		box_f.pack_start rapport, true, true, 2
		box_f.pack_start scroll, true, true, 2
		
		@frame.add box_f
		
		@frame2 = Gtk::Frame.new t.dashboard.farming_book
		@cahier = Cahier_box.new(@window)
		@frame2.add @cahier
		vbox.pack_start @frame, true, true, 2
		vbox.pack_start @frame2, true, true, 2
		
		add vbox
	
	end
	
	def rapport
	
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)    	
  	scroll.add @rapport
  	
  	scroll
	
	end
	
	def refresh
		
		min_max = def_annee
		@legende_graph = []
		if min_max[0] and min_max[1]
			i=1
			(min_max[0]..min_max[1]).to_a.each do |a|
				@legende_graph << a if i<5
				i+=1
			end
		
			@rapport.refresh min_max[0], min_max[1]
		
			Thread.new {
				sleep 0.1
				@gr.refresh @rapport.serie, @couleur_graph, @legende_graph, @unite
			}
			
			@cahier.refresh
		end
		
	end
	
	def def_annee
		
		min = nil
		max = nil
		doc = Cultureligne.find_by_sql("SELECT MIN(date) AS min, MAX(date) AS max FROM culturelignes").first
		if doc.min and doc.max
			if doc.min.is_a?(String)
				min = Date.parse(doc.min).year
				max = Date.parse(doc.max).year
			else
				min = doc.min.year
				max = doc.max.year
			end
		end
		return [min, max]
		
	end
	
	def imprimer
	
		tableau = []
		tableau << ["Mois", "Dép.n-3", "Rec.n-3", "Bén.n-3", "Dép.n-2", "Rec.n-2", "Bén.n-2", "Dép.n-1", "Rec.n-1", "Bén.n-1", "Dép.n", "Rec.n", "Bén.n"]
		@rapport.list_store.each {|model, path, iter|
			m = @rapport.list_store.get_iter(path)
			tableau << [m[0],m[1],m[2],m[3],m[4],m[5],m[6],m[7],m[8],m[9],m[10],m[11],m[12]]
		}
		
		image = @window.config_db.chemin_temp+'/graph.png'
		@gr.save image
		tmp = @window.config_db.chemin_temp+'/stat.pdf'
		if File.exist?(image)
			EditionResultat.new(image, tableau, tmp, @window)	
		
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{tmp}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start","#{tmp}"
				else
				
				end
			end
		end
	
	end

end
