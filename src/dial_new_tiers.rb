# coding: utf-8

class DialNewTiers < Gtk::Dialog
	
	attr_reader :tiers_window
	
	def initialize window, id=0
		
		super("Nouvel article", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 1000, 800
		 
		@tiers_window = Tiers_box.new window, true
		@tiers_window.refresh id
		
		self.vbox.pack_start @tiers_window, true, true, 3
		
		self.vbox.show_all
	
	end

end		
