# coding: utf-8

class SearchTiers < Gtk::Dialog

	attr_reader :liste
	
	def initialize window, term, type
		
		super("Recherche tiers", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 900, 500
		
		@liste = ListeTiers_box.new window, true
		
		@liste.view.signal_connect ("row-activated") { |view, path, column|
			# renvoi le signal OK si double clic
			self.response(Gtk::Dialog::RESPONSE_OK)
		}
		
		@liste.add_new.signal_connect( "clicked" ) {
			dial_tiers = DialNewTiers.new window
			dial_tiers.run { |response| 
				code = nil
				if response.eql?(-5)
					dial_tiers.tiers_window.validate
					code = dial_tiers.tiers_window.tiers.nom
				end
				dial_tiers.destroy
				@liste.refresh type, code.to_s
				un_seul?
			}
		}
		
		@liste.filtre.active = type
		@liste.refresh type, term  # 0=> clients, 1 => fournisseurs
		
		@liste.search.text = term
		
		self.vbox.pack_start @liste, true, true, 3
		
		self.vbox.show_all
		
		#Thread.new {
		#	sleep 0
		#	un_seul?
		#}
	
	end
	
	# Si un seul dans la liste, sélection automatique et fermeture du dialogue.
	def un_seul?
		i = 0
		npath = nil
		@liste.view.model.each { |model, path, iter| 
			i += 1
			npath = path
		}
		if i.eql?(1)
			@liste.view.set_cursor(npath, nil, false)
			response(Gtk::Dialog::RESPONSE_OK)
		end
	end


end		
