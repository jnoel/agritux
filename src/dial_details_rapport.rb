# coding: utf-8

class DialDetailRapport < Gtk::Dialog
	
	def initialize annee, window
		
		super("Detail de l'année #{annee}", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 800, 400
		
		self.vbox.pack_start tableau_mouvements, true, true, 3
		
		self.vbox.show_all
		
		remplir_mouvement annee
	
	end
	
	def tableau_mouvements
	
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC
		scroll.shadow_type = Gtk::SHADOW_NONE
		
		# 															  id      type    date    Qtite   unite   PrixU   PrixTot Raison Type_id   Article
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String, Integer, String)
		@view = Gtk::TreeView.new(@list_store)
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new("Type", renderer_left, :text => 1)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date", renderer_left, :text => 2)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 9)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Quantité", renderer_right, :text => 3)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Unité", renderer_right, :text => 4)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix unitaire", renderer_right, :text => 5)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix total", renderer_right, :text => 6)
		col.resizable = true
		@view.append_column(col)
		
		scroll.add @view
		
		scroll
	
	end
	
	def remplir_mouvement annee
	
		@view.model.clear
		
		date_deb = Date.parse("01/01/#{annee}")
		date_fin = Date.parse("31/12/#{annee}")
		
		# Dépenses
		depenses = Articleentree.where("date>='#{date_deb}' AND date<='#{date_fin}' AND article_id>0" ).order("date DESC")
		iter_depenses = @list_store.append nil
		iter_depenses[1] = t.dashboard.outgoings
		depenses.each do |h| 	
			iter = @list_store.append iter_depenses
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = "%.2f" % h.quantite
			iter[4] = h.unite
			iter[5] = "%.2f" % h.prix_unitaire
			iter[6] = "%.2f €" % h.prix_total
			iter[7] = h.raison
			iter[8] = 1
			iter[9] = h.article.designation
		end
		
		# Recettes
		recettes = Cultureligne.where(:date => date_deb..date_fin, :article_id => -1).order("date DESC")
		iter_recettes = @list_store.append nil
		iter_recettes[1] = t.dashboard.incomes
		recettes.each do |h| 	
			iter = @list_store.append iter_recettes
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = "%.2f" % h.quantite
			iter[4] = h.unite
			iter[5] = "%.2f" % h.prix_unitaire
			iter[6] = "%.2f €" % h.prix_total
			iter[7] = h.raison
			iter[8] = 2
			iter[9] = "#{h.article_designation} (#{h.culture.designation})"
		end
		
		# Subventions globale
		subventions = Subvention.where(:date => date_deb..date_fin).order("date DESC")
		iter_subventions = @list_store.append nil
		iter_subventions[1] = t.dashboard.farm_grants
		subventions.each do |h| 	
			iter = @list_store.append iter_subventions
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = ""
			iter[4] = ""
			iter[5] = ""
			iter[6] = "%.2f €" % h.montant
			iter[7] = h.raison
			iter[8] = 2
			iter[9] = h.designation
		end
		
		@view.expand_all
		
	end

end		

