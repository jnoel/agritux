# coding: utf-8

class DialGestionDb < Gtk::Dialog
	
	def initialize window
		
		super(t.gestion_db.title, window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		set_default_size 700, 500
		
		@window = window
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start tableau_bases, true, true, 2
		hbox.pack_start boutons, false, false, 2
		self.vbox.add hbox
		self.vbox.show_all
		
		remplir_bases
		
	end
	
	def tableau_bases
		
		@bases = Gtk::TreeView.new Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String, String)
		@bases.signal_connect ("row-activated") { |view, path, column|
			ligne = @bases.selection.selected
			if ligne[8]=="postgresql"
				param_base_postgres ligne[0]
			elsif ligne[8]=="sqlite3"
				param_base_sqlite ligne[0]
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new(t.gestion_db.name, renderer_left, :text => 7)
		col.sort_column_id = 7
		@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.gestion_db.type, renderer_left, :text => 8)
		col.sort_column_id = 8
		@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.gestion_db.host, renderer_left, :text => 2)
		col.sort_column_id = 2
		@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.gestion_db.base, renderer_left, :text => 3)
		col.sort_column_id = 3
		@bases.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @bases
    	
	end
	
	def boutons
		
		vbox = Gtk::VBox.new false, 2
		
		add = Gtk::Button.new t.gestion_db.new_db
		add.signal_connect( "clicked" ) {
			choix_db
		}
		remove = Gtk::Button.new t.gestion_db.remove_db
		remove.signal_connect( "clicked" ) {
			supprimer_base @bases.model.get_value @bases.selection.selected, 0
		}
		
		vbox.pack_start add, false, false, 2
		vbox.pack_start remove, false, false, 2
		
		vbox
		
	end
	
	def supprimer_base id
		
		if id
			dialog = Gtk::MessageDialog.new(self, 
		                              Gtk::Dialog::DESTROY_WITH_PARENT,
		                              Gtk::MessageDialog::QUESTION,
		                              Gtk::MessageDialog::BUTTONS_YES_NO,
		                              t.gestion_db.confirm_remove)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
					conf = @window.config_db.bases[id]
					@window.config_db.bases.delete conf
					@window.config_db.save_config global=true
					remplir_bases
					@window.login.refresh
			end 
			dialog.destroy
		end
		
	end
	
	def remplir_bases
		
		@bases.model.clear
		
		bases = @window.config_db.bases
		
		if bases
			i=0
			bases.each do |b|
				iter = @bases.model.append nil
				iter[0] = i
				iter[1] = b["adapter"]
				iter[2] = b["host"]
				iter[3] = b["database"]
				iter[4] = b["port"]
				iter[5] = b["username"]
				iter[6] = b["password"]
				iter[7] = b["nom"].to_s
				iter[8] = b["adapter"]
				i += 1
			end
		end
		
	end
	
	def param_base_postgres id
	
		dial_param_db = DialParamDb.new @window, id
		dial_param_db.run { |response| 
			if response==-5
				if id>-1
					base = @window.config_db.bases[id]
					base["nom"] = dial_param_db.nom.text
					base["host"] = dial_param_db.host.text
					base["database"] = dial_param_db.database.text
					base["port"] = dial_param_db.port.text
					base["username"] = dial_param_db.username.text
					base["password"] = dial_param_db.password.text
					base["chemin_documents"] = dial_param_db.chemin_documents.text
					base["default_user"] = ""
					base["default_user_password"] = ""
				else
					id = 0
					if @window.config_db.bases
						id = @window.config_db.bases.count
					else
						@window.config_db.bases = []
					end
					base = {  "nom" => dial_param_db.nom.text,
										"adapter" => "postgresql",
										"host" => dial_param_db.host.text,
										"database" => dial_param_db.database.text,
										"port" => dial_param_db.port.text,
										"username" => dial_param_db.username.text,
										"password" => dial_param_db.password.text,
										"chemin_documents" => (dial_param_db.chemin_documents.text.empty? ? @window.config_db.chemin_temp : dial_param_db.chemin_documents.text),
										"default_user" => "",
										"default_user_password" => "",
										"id" => id
								  }
					@window.config_db.bases << base
				end
				@window.config_db.save_config global=true
				remplir_bases
				@window.login.refresh
			end
			dial_param_db.destroy 			
		}
	
	end
	
	def choix_db
		dialogue = DialChooseDbType.new @window
		response = dialogue.run
		case response
			when Gtk::Dialog::RESPONSE_OK
				if dialogue.choix=="postgres"
					dialogue.destroy
					param_base_postgres -1
				elsif dialogue.choix=="sqlite"
					dialogue.destroy
					param_base_sqlite -1
				end
			else
				dialogue.destroy
		end 
	end
	
	def param_base_sqlite id
		dial_param_db = DialParamDbSqlite.new @window, id
		dial_param_db.run { |response| 
			if response==-5
				if id>-1
					base = @window.config_db.bases[id]
					base["nom"] = dial_param_db.nom.text
					base["database"] = dial_param_db.database.text
					base["chemin_documents"] = dial_param_db.chemin_documents.text
					base["default_user"] = ""
					base["default_user_password"] = ""
				else
					id = 0
					if @window.config_db.bases
						id = @window.config_db.bases.count
					else
						@window.config_db.bases = []
					end
					
					chemin = ""
					if dial_param_db.chemin_documents.text.empty?
						chemin = File.join(@window.config_db.chemin_temp,dial_param_db.database.text)
					else
						chemin = dial_param_db.chemin_documents.text
					end
					Dir.mkdir chemin unless Dir.exist?(chemin)
					base = {  "nom" => dial_param_db.nom.text,
										"adapter" => "sqlite3",
										"database" => File.join(@window.config_db.chemin_db, dial_param_db.database.text),
										"chemin_documents" => chemin,
										"default_user" => "",
										"default_user_password" => "",
										"id" => id
								  }
					@window.config_db.bases << base
				end
				@window.config_db.save_config global=true
				remplir_bases
				@window.login.refresh
			end
			dial_param_db.destroy 			
		}
	end

end
