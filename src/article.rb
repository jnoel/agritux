# coding: utf-8

class Article_box < Gtk::VBox

	attr_reader :article
	MAX_SPIN = 999999

	def initialize window, dial=false
	
		super(false, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@image_chargement = Gdk::Pixbuf.new "resources/icons/chargement.png"
		@image_nograph = Gdk::Pixbuf.new "resources/icons/nograph.png"
		@activer_combo = false
		
		@info1 = Gtk::Entry.new
		@info2 = Gtk::Entry.new
		@description = Gtk::TextView.new
		@datecreation = Gtk::Calendar.new
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new t.global.validate
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new t.global.cancel
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::HBox.new false, 2
		hboxsupprimer.add Gtk::Image.new( "./resources/icons/trash-full.png" )
		hboxsupprimer.add Gtk::Label.new t.global.delete
		@supprimer.add hboxsupprimer
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = t.good.new
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start @supprimer, false, false, 3
		hbox.pack_start align1, true, true, 3
		
		vbox.pack_start( hbox, false, false, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		# Objets
		@code = Gtk::Entry.new
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new @groupe_model
		renderer_left = Gtk::CellRendererText.new
		@designation = Gtk::Entry.new		
		@description = Gtk::TextView.new	
		@image = Gtk::Image.new
		@unite = Gtk::ComboBox.new
		@prix = Gtk::Entry.new
		@stock = Gtk::Entry.new
		@stock.sensitive = false
		scroll_desc = Gtk::ScrolledWindow.new
		
		# Propriétés
		@description.wrap_mode = Gtk::TextTag::WRAP_WORD
		@code.width_chars = 8
		renderer_left.xalign = 0
		scroll_desc.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_desc.shadow_type = Gtk::SHADOW_IN
		
		# Agencement		
		vbox = Gtk::VBox.new false, 3
		
		table = Gtk::Table.new 3, 9, false
		
		table.attach( Gtk::Label.new(t.global.code), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @code, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		@groupe.pack_start renderer_left, true
		@groupe.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new(t.global.group), 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @groupe, 3, 4, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.unit), 4, 5, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @unite, 5, 6, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.good.current_stock), 6, 7, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @stock, 7, 8, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		table.attach( Gtk::Label.new(t.global.designation), 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @designation, 1, 6, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::FILL, 5, 5 )	
		
		table.attach( Gtk::Label.new(t.global.price), 6, 7, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( @prix, 7, 8, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )	
		
		vbox.pack_start table, false, false, 3
		
		align_desc = Gtk::Alignment.new 0, 0, 0, 0
		align_desc.add Gtk::Label.new( t.global.description )
		
		scroll_desc.add @description
		#vbox.pack_start align_desc, false, false, 3
		#vbox.pack_start scroll_desc, true, true, 3
		
		vbox.pack_start notebook, true, true, 3
		
		vbox
	
	end
	
	def notebook
	
		# Objets	
		@note = Gtk::TextView.new
		renderer_left = Gtk::CellRendererText.new
		@ged = Ged_box.new @window, "articles"
		@graph = Gtk::Image.new
		new_entree = Gtk::Button.new
		new_sortie = Gtk::Button.new
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new
		vbox_mouvements = Gtk::VBox.new false, 2
		hbox_mouvements = Gtk::HBox.new false, 2
		align_mouvements = Gtk::Alignment.new 1, 0, 0, 0
		hbox_new_entree = Gtk::HBox.new false, 2
		hbox_new_entree.add Gtk::Image.new( "./resources/icons/add.png" )
		hbox_new_entree.add Gtk::Label.new t.good.new_input
		new_entree.add hbox_new_entree
		hbox_new_sortie = Gtk::HBox.new false, 2
		hbox_new_sortie.add Gtk::Image.new( "./resources/icons/minus.png" )
		hbox_new_sortie.add Gtk::Label.new t.good.new_output
		new_sortie.add hbox_new_sortie

		# Propriétés des objets
		@note.wrap_mode = Gtk::TextTag::WRAP_WORD
		scroll_note.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_note.shadow_type = Gtk::SHADOW_NONE	
		new_entree.signal_connect("clicked") { nouvelle_entree}
		new_sortie.signal_connect("clicked") { nouvelle_sortie}

		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du conteneur Mouvements
		align_mouvements.add hbox_mouvements
		hbox_mouvements.add new_entree
		hbox_mouvements.add new_sortie
		vbox_mouvements.pack_start align_mouvements, false, false, 2
		vbox_mouvements.pack_start tableau_mouvements, true, true, 2
		
		# Agencement du notebook
		notebook.append_page vbox_mouvements, Gtk::Label.new("#{t.good.input}/#{t.good.output}")
		notebook.append_page scroll_note, Gtk::Label.new(t.global.notes)

		# Renvoie
		notebook
		
	end
	
	def tableau_mouvements
	
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll.shadow_type = Gtk::SHADOW_NONE
		
		# 															  id      type    date    Qtite   unite   PrixU   PrixTot Raison Type_id   Corbeille
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String, Integer, Gdk::Pixbuf)
		@view = Gtk::TreeView.new(@list_store)
		
		# Les événements
		@view.signal_connect ("button-release-event") { |tree,event|
			# Si clic gauche
			e = npath = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				#si clic sur colonne supprimer
				if colonne.eql?(@view.get_column(7))
					ligne = @view.model.get_iter(e[0])
					supprime_ligne ligne if [1,2].include?(ligne[8])
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new(t.global.type, renderer_left, :text => 1)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.date, renderer_left, :text => 2)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.quantity, renderer_right, :text => 3)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.unit, renderer_right, :text => 4)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.unit_price, renderer_right, :text => 5)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.total_price, renderer_right, :text => 6)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.reason, renderer_left, :text => 7)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		# Colonne pour la corbeille
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0.5
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 9)
		col.resizable = true
		@view.append_column(col)
		
		scroll.add @view
		
		scroll
	
	end
	
	def refresh id=0
	
		@id = id
		@code.text = ""
		@designation.text = ""
		@note.buffer.text = ""
		@article = nil
		@supprimer.sensitive = id>0
		
		if id>0 then
			@frame.label = "Article n° " + id.to_s
			im = nil
			format = ["jpg", "JPG", "jpeg", "JPEG", "gif", "GIF", "png", "PNG"]
			format.each do |f|
				if File.exist?("./resources/articles/#{@id}.#{f}")
					im = "./resources/articles/#{@id}.#{f}"
				end
			end
		
			if im
				@image.pixbuf = Gdk::Pixbuf.new(im, width = 150, height = 150)
			else
				@image.clear
			end
			@article = Article.find(id)
			if @article then
				@code.text = @article.code
				@code.sensitive = false
				@designation.text = @article.designation
				@note.buffer.text = @article.note.to_s
				@description.buffer.text = @article.description.to_s
				remplir_groupe @article.articlegroupe_id.to_i
				@prix.text = "%.2f" % @article.prix
				@stock.text = "%.2f" % @article.stock
				remplir_mouvement
				remplir_unite @article.unite.to_s
				#@unite.sensitive = false
			else
				@window.message_erreur @article.error
			end
			
			#@ged.refresh @id
			
		else
			@frame.label = "Nouvel article"
			@article = Article.new
			@image.clear
			@code.sensitive = true
			remplir_groupe
			@description.buffer.text = ""
			remplir_unite
			@unite.sensitive = true
			@view.model.clear
		end
	
	end
	
	def remplir_mouvement
	
		@view.model.clear
		
		# Entrée de marchandises
		entrees = Articleentree.where(:article_id => @id).order("date DESC")
		iter_entree = @list_store.append nil
		iter_entree[1] = t.good.input
		entrees.each do |h| 	
			iter = @list_store.append iter_entree
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = "%.2f" % h.quantite
			iter[4] = h.unite
			iter[5] = "%.2f" % h.prix_unitaire
			iter[6] = "%.2f" % h.prix_total
			iter[7] = h.raison
			iter[8] = 1
			iter[9] = Gdk::Pixbuf.new("./resources/icons/trash-full.png", 24, 24)
		end
		
		# Sortie de marchandises
		sorties = Articlesortie.where(:article_id => @id).order("date DESC")		
		iter_sorties = @list_store.append nil
		iter_sorties[1] = t.good.output
		sorties.each do |h| 	
			iter = @list_store.append iter_sorties
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = "%.2f" % h.quantite
			iter[4] = h.unite
			iter[5] = ""
			iter[6] = ""
			iter[7] = h.raison
			iter[8] = 2
			iter[9] = Gdk::Pixbuf.new("./resources/icons/trash-full.png", 24, 24)
		end
		
		# Utilisation sur parcelle
		utilisations = Cultureligne.where(:article_id => @id).order("date DESC")		
		iter_utilisation = @list_store.append nil
		iter_utilisation[1] = t.good.farming_uses
		utilisations.each do |h| 	
			iter = @list_store.append iter_utilisation
			iter[0] = h.id
			iter[2] = h.date.strftime("%d/%m/%Y")
			iter[3] = "%.2f" % h.quantite
			iter[4] = h.unite
			iter[5] = "%.2f" % h.prix_unitaire
			iter[6] = "%.2f" % h.prix_total
			iter[7] = h.raison
			iter[8] = 3
		end
		
		@view.expand_all
		
	end
	
	def remplir_unite x=""
		@window.unites.count.times do
			@unite.remove_text 0
		end
		unit = @window.unites
		i=0
		unit.each do |u|
			@unite.append_text u
			if x.eql?(u)
				@unite.active = i
			end
			i += 1
		end
		@unite.active = 0 if @unite.active.eql?(-1)
	end
	
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		groupesp = Articlegroupe.order(:designation)
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if gr.parent_id.eql?(0)
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end						
					groupesp.each do |gr2|
						if gr2.parent_id.eql?(gr.id)
							iter2 = @groupe_model.append iter
							iter2[0] = gr2.id.to_i
							iter2[1] = gr2.designation
					
							if !nom.nil?
								active_iter = iter2 if nom.eql?(gr2.id)		
							end	
						end
					end
				end
			end
		
			if !active_iter.nil? then @groupe.active_iter = active_iter end
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def supprime_ligne ligne
	
		suppr = false
		dialog = Gtk::MessageDialog.new(@window, 
			                          Gtk::Dialog::DESTROY_WITH_PARENT,
			                          Gtk::MessageDialog::QUESTION,
			                          Gtk::MessageDialog::BUTTONS_YES_NO,
			                          t.good.confirm_delete_line)
		response = dialog.run
		case response
			when Gtk::Dialog::RESPONSE_YES
			suppr = true
		end 
		dialog.destroy

		if suppr
			if ligne[8]==1
				l = Articleentree.find(ligne[0])
				l.destroy
				@stock.text = "%.2f" % (@stock.text.to_f-ligne[3].to_f)
				@article.stock = @stock.text.to_f
				@article.save
				remplir_mouvement
			elsif ligne[8]==2
				l = Articlesortie.find(ligne[0])
				l.destroy
				@stock.text = "%.2f" % (@stock.text.to_f+ligne[3].to_f)
				@article.stock = @stock.text.to_f
				@article.save
				remplir_mouvement
			end
		end
	
	end
	
	def nouvelle_entree
	
		if validate(false)
			dial_entree = DialEntree.new @window
			dial_entree.date.text = DateTime.now.strftime("%d/%m/%Y")
			dial_entree.quantite.text = "1"
			dial_entree.prix_unitaire.text = @prix.text
			dial_entree.run { |response| 
				if response.eql?(-5)
					begin
						date = Date.parse(dial_entree.date.text)
						quantite = dial_entree.quantite.text.to_f.round(2)
						pu = dial_entree.prix_unitaire.text.to_f.round(2)
						total = (quantite*pu).round(2)
						raison = dial_entree.raison.text
						Articleentree.create :date => date, :quantite => quantite, :prix_unitaire => pu, :prix_total => total, :raison => raison, :article_id => @article.id, :unite => @article.unite
						@stock.text = "%.2f" % (@stock.text.to_f+quantite)
						@article.stock = @stock.text.to_f
						@article.save
						remplir_mouvement
						dial_entree.destroy
					rescue
						dial_entree.destroy
						@window.message_erreur t.good.error_date
					end
				else
					dial_entree.destroy
				end
			}
		end
	
	end
	
	def nouvelle_sortie
	
		if validate(false)
			dial_sortie = DialSortie.new @window
			dial_sortie.date.text = DateTime.now.strftime("%d/%m/%Y")
			dial_sortie.quantite.text = "1"
			dial_sortie.run { |response| 
				if response.eql?(-5)
					begin
						date = Date.parse(dial_sortie.date.text)
						quantite = dial_sortie.quantite.text.to_f.round(2)
						raison = dial_sortie.raison.text
						Articlesortie.create :date => date, :quantite => quantite, :raison => raison, :article_id => @article.id, :unite => @article.unite
						@stock.text = "%.2f" % (@stock.text.to_f-quantite)
						@article.stock = @stock.text.to_f
						@article.save
						remplir_mouvement
						dial_sortie.destroy
					rescue
						dial_sortie.destroy
						@window.message_erreur t.good.error_date
					end
				else
					dial_sortie.destroy
				end	
			}
		end
	
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		if !(@id>0) then
			if @code.text.empty? then
				error += "Vous devez saisir un code pour cet article\n"
			else
				error += "Le code article saisi existe déjà. Veuillez en saisir un autre.\n" if Article.where("code=?", @code.text).count>0
			end
		end
		error += @designation.text.empty? ? t.good.error_designation_empty : ""
		
		# Si pas d'erreurs on continue
		if error.empty? then

			groupe = @groupe_model.get_value(@groupe.active_iter,0).nil? ? 0 : @groupe_model.get_value(@groupe.active_iter,0)
		
			# sauvegarde des données de l'article
			@article.designation = @designation.text
			@article.code = @code.text
			@article.description = @description.buffer.text
			@article.note = @note.buffer.text
			@article.articlegroupe_id = groupe
			@article.unite = @unite.active_text
			@article.prix = @prix.text.to_f

			res = @article.save
		
			quit if quitter
		
		else
			@window.message_erreur error
		end
		return res
	
	end
	
	def supprimer
	
		if @id>0
			suppr = false
			dialog = Gtk::MessageDialog.new(@window, 
					                        Gtk::Dialog::DESTROY_WITH_PARENT,
					                        Gtk::MessageDialog::QUESTION,
					                        Gtk::MessageDialog::BUTTONS_YES_NO,
					                        t.good.confirm_delete)
			response = dialog.run
			case response
				when Gtk::Dialog::RESPONSE_YES
				suppr = true
			end 
			dialog.destroy
		
			if suppr
		
				lignes = Cultureligne.where(:article_id => @id)
				entrees = Articleentree.where(:article_id => @id)
				sorties = Articlesortie.where(:article_id => @id)
				if (lignes.count.eql?(0) and entrees.count.eql?(0) and sorties.count.eql?(0))
					@article.destroy
					quit
				else
					@window.message_erreur t.good.error_delete
				end
						
			end
		end
	
	end
	
	def combo_sensitive status
		@annee.sensitive = status
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def quit
	
		@window.liste_articles.refresh
		@window.affiche @window.liste_articles
		@window.liste_articles.focus
	
	end
	
end

