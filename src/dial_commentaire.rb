# coding: utf-8

class DialCommentaire < Gtk::Dialog

	attr_reader :raison, :methode, :notes
	
	def initialize window
		
		super("Sortie d'articles", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ], [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ])
		
		#set_default_size 900, 300
		
		table = Gtk::Table.new 3, 2, false
		
		#raison
		label_raison = Gtk::Alignment.new 0, 0.5, 0, 0
		label_raison.add Gtk::Label.new("Raison du traitement:")
		table.attach( label_raison, 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		@raison = Gtk::Entry.new 
		table.attach( @raison, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, 2, 2 )
		#méthode
		label_methode = Gtk::Alignment.new 0, 0.5, 0, 0
		label_methode.add Gtk::Label.new("Méthode du traitement:")
		table.attach( label_methode, 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		@methode = Gtk::Entry.new
		table.attach( @methode, 1, 2, 1, 2, Gtk::FILL, Gtk::FILL, 2, 2 )
		#notes
		label_notes = Gtk::Alignment.new 0, 0.5, 0, 0
		label_notes.add Gtk::Label.new("Notes:")
		table.attach( label_notes, 0, 1, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )
		@notes = Gtk::Entry.new
		table.attach( @notes, 1, 2, 2, 3, Gtk::FILL, Gtk::FILL, 2, 2 )
		
		self.vbox.pack_start table, true, true, 3
		
		self.vbox.show_all
	
	end

end		

