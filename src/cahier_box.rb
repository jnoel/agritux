# coding: utf-8

class Cahier_box < Gtk::VBox

	attr_reader :view
	
	def initialize window
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		agencement
	
	end
	
	def agencement
	
		@annee = Gtk::ComboBox.new
		remplir_annee DateTime.now.year
		
		@annee.signal_connect('changed') {
			refresh @parcelle_id, @annee.active_text.to_i
		}
		
		hbox = Gtk::HBox.new false, 2
		hbox.pack_start Gtk::Label.new("#{t.global.year} n :"), false, false, 2 
		hbox.pack_start @annee, false, false, 2 
		
		print = Gtk::Button.new
		hbox_print = Gtk::HBox.new false, 2
		hbox_print.add Gtk::Image.new( "./resources/icons/print.png" )
		label_print = Gtk::Label.new t.dashboard.print_book
		hbox_print.add label_print
		print.add hbox_print
		hbox.pack_start print, false, false, 2 
		
		print.signal_connect ("clicked") { imprimer }
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add hbox
		
		self.pack_start align_button, false, false, 2
		self.pack_start tableau_cultures, true, true, 2
	
	end
	
	def tableau_cultures
	
		scroll_cahier = Gtk::ScrolledWindow.new
		scroll_cahier.set_policy Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC
		scroll_cahier.shadow_type = Gtk::SHADOW_NONE
		
		# 															id      cycle    date date_string article_id article unite quantite prix_unitaire prix_total image_note trash, raison, methode, note, culture, articlegroupe, parcelle)
		@list_store = Gtk::TreeStore.new(Integer, Integer, Date, String, Integer, String, String, String, String, String, Gdk::Pixbuf, Gdk::Pixbuf, String, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		renderer_center = Gtk::CellRendererText.new
		renderer_center.background = "white"
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new(t.global.parcel, renderer_left, :text => 17)
		col.sort_column_id = 17
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.cycle, renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.farming, renderer_left, :text => 15)
		col.sort_column_id = 15
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.date, renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.group, renderer_left, :text => 16)
		col.sort_column_id = 16
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.good, renderer_left, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.reason, renderer_left, :text => 12)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.methode, renderer_left, :text => 13)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.notes, renderer_left, :text => 14)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.quantity, renderer_right, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.unit, renderer_center, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		scroll_cahier.add @view
	
	end
	
	def refresh parcelle_id=nil, annee=nil
		
		@parcelle_id = parcelle_id
		
		annee = (@annee.active_text.nil? ? DateTime.now.year : @annee.active_text)
		
		cultures = nil
		datedeb = Date.parse("01/01/#{annee}")
		datefin = Date.parse("31/12/#{annee}")
		
		if !parcelle_id.nil?
			cultures = Cultureligne.includes(:culture, :article, :parcelle).where(:date => datedeb..datefin, :parcelle_id => parcelle_id)
			cultures = cultures.where("(articles.articlegroupe_id IN (1,2,4,5) OR article_id = ?)", -1).references(:articlegroupe_id, :article_id).order(:date, "culturelignes.id")
		else
			cultures = Cultureligne.includes(:culture, :article, :parcelle).where(:date => datedeb..datefin)
			cultures = cultures.where("(articles.articlegroupe_id IN (1,2,4,5) OR article_designation = ?)", "Vente").references(:parcelle_id, :articlegroupe_id, :article_id).order(:date, "culturelignes.id")
		end
		
		remplir_cahier cultures
	
	end
	
	def remplir_cahier res
	
		@view.model.clear
		res.each { |h| 
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.cycle
			iter[2] = h.date
			iter[3] = h.date.strftime("%d/%m/%Y")
			iter[4] = h.article_id
			iter[5] = h.article_designation
			iter[6] = h.unite
			iter[7] = "%.2f" % h.quantite
			iter[12] = h.raison
			iter[13] = h.methode
			iter[14] = h.note
			iter[15] = h.culture.designation
			iter[16] = (h.article_id.eql?(-1) ? "Vente" : h.article.articlegroupe.designation )
			iter[17] = h.parcelle.designation
		}
	
	end
	
	def remplir_annee annee
		
		doc = Cultureligne.find_by_sql("SELECT MIN(date) AS min, MAX(date) AS max FROM culturelignes").first
		if doc.min and doc.max
			if doc.min.is_a?(String)
				min = Date.parse(doc.min).year
				max = Date.parse(doc.max).year
			else
				min = doc.min.year
				max = doc.max.year
			end
			i=0
			(min..max).to_a.reverse.each do |a|
				@annee.append_text a.to_s
				@annee.active = i if a==annee
				i+=1
			end
			@annee.active=0 if @annee.active.eql?(-1)
		end
		
	end
	
	def imprimer
		
		if Cultureligne.first
			fichier = (@parcelle_id ? "parcelle#{@parcelle_id}.pdf" : "cahier_culture.pdf")
			chemin = "#{@window.config_db.chemin_temp}/#{fichier}"
			EditionCahier.new @annee.active_text.to_i, chemin, @window

			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open", chemin
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start", chemin
				else
			
				end
			end
		end
		
	end
	
end
