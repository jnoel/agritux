# coding: utf-8

class ListeParcelles_box < Gtk::VBox

	attr_reader :view, :search_gencode, :add_new
	
	def initialize window, research = false
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@research = research
		
		@position = [0,0]
		 
		agencement
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new t.parcel.new
		@add_new.add hboxajout
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add @add_new
		frame = Gtk::Frame.new t.global.parcels
		@compteur = Gtk::Label.new 
		
		hbox = Gtk::HBox.new false, 3
		@search_gencode = Gtk::Entry.new
		@search_gencode.primary_icon_stock = Gtk::Stock::FIND
		@search_gencode.secondary_icon_stock = Gtk::Stock::CLEAR
		
		# Agencement
		hbox.add @search_gencode
		hbox.add align_button
		
		vbox.pack_start hbox, false, false, 3 
		vbox.pack_start tableau_parcelles, true, true, 3 
		vbox.pack_start @compteur, false, false, 3 

		frame.add vbox
		
		if !@research
			@add_new.signal_connect( "clicked" ) {
				@window.parcelle.refresh
				@window.affiche @window.parcelle
			}
		end
		
		@search_gencode.signal_connect('activate') {
			refresh @search_gencode.text
		}
		
		add frame
	
	end
	
	def tableau_parcelles
	
		# list_store (id, nom, taille, photo, cultures)
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Gdk::Pixbuf, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				@window.parcelle.refresh @view.model.get_value @view.selection.selected, 0
				@window.affiche @window.parcelle
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		# Colonne pour la photo
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 4)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.code, renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.global.designation, renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.parcel.current_farming, renderer_left, :text => 5)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new(t.parcel.size, renderer_right, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh search=nil
		
		search = @search_gencode.text unless @search_gencode.text.empty?
		
		parcelles = Parcelle.order(:id)
		if search
			parc = Parcelle.arel_table
			parcelles = parcelles.where(parc[:designation].matches("%#{search}%"))
		end
		@compteur.text = "#{t.global.count_elements} : #{parcelles.count}"

		if parcelles then	
			remplir parcelles
		end	
	
	end
	
	def remplir res

		@list_store.clear
		
		res.each { |h| 	
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.code
			iter[2] = h.designation
			iter[3] = "#{h.taille} m²"
			iter[4] = Gdk::Pixbuf.new("./resources/icons/parcelle48.png", 48, 48)
			cultures_en_cours = []
			cultures = Cultureligne.select("culture_id as id, min(article_id) as min_article_id").where(:parcelle_id => h.id).group("culture_id")
			cultures.each do |c|
				if c.min_article_id.to_i>-2
					cultures_en_cours << Culture.find(c.id).designation
				end
			end
			iter[5] = cultures_en_cours.join(", ")
		}
			
	end
	
	def focus
		@search_gencode.grab_focus
	end
	
end

