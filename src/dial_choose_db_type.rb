# coding: utf-8

class DialChooseDbType < Gtk::Dialog

	attr_reader :choix
	
	def initialize window
		
		super("Choisissez le type de base de données", window, Gtk::Dialog::DESTROY_WITH_PARENT, [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ])
		
		set_default_size 700, 500
		
		@choix = nil
		
		align_intro = Gtk::Alignment.new 0, 0.5, 0, 0
		intro = Gtk::Label.new( "<b>Vous avez le choix entre deux types de base de données :</b>" )
		intro.use_markup = true
		align_intro.add intro
		align_intro_sqlite = Gtk::Alignment.new 0, 0, 0, 0
		intro_sqlite = Gtk::Label.new( "<b><u>Débutants :</u></b> base de données <b>Sqlite</b>\n\n"+
																	 "Ce type de base est conseillé si vous comptez utiliser\n"+
																	 "AgriTux uniquement depuis cet ordinateur.\n\n"+
																	 "Elle sera crée automatiquement.\n" )
		intro_sqlite.use_markup = true
		align_intro_sqlite.add intro_sqlite
		align_intro_postgres = Gtk::Alignment.new 0, 0, 0, 0
		intro_postgres = Gtk::Label.new( "<b><u>Experts :</u></b> base de données <b>PostgreSQL</b>\n\n"+
																		 "Vous devez choisir ce type de base de données si vous\n"+
																		 "voulez fonctionner en mode réseau.\n\n"+
														 				 "Il vous faudra auparavent avoir une base postgresql prête\n"+
														 				 "quelque part sur votre réseau." )
		intro_postgres.use_markup = true
		align_intro_postgres.add intro_postgres
		postgres_bt = Gtk::Button.new
		postgres_bt.add Gtk::Image.new( "./resources/icons/postgres.png" )
		sqlite_bt = Gtk::Button.new
		sqlite_bt.add Gtk::Image.new( "./resources/icons/sqlite.png" )
		
		table = Gtk::Table.new 2, 2, false
		table.column_spacings = 30
		
		i=0
		table.attach align_intro, 0, 1, i, i+1
		
		i+=1
		table.attach align_intro_sqlite, 0, 1, i, i+1
		table.attach align_intro_postgres, 1, 2, i, i+1
		
		i+=1
		table.attach sqlite_bt, 0, 1, i, i+1
		table.attach postgres_bt, 1, 2, i, i+1
		
		postgres_bt.signal_connect("clicked") {
			@choix = "postgres"
			# renvoi le signal OK
			response(Gtk::Dialog::RESPONSE_OK)
		}
		
		sqlite_bt.signal_connect("clicked") {
			@choix = "sqlite"
			# renvoi le signal OK
			response(Gtk::Dialog::RESPONSE_OK)
		}
		
		self.vbox.pack_start table, true, true, 3
		
		self.vbox.show_all

	end

end
