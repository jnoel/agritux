# coding: utf-8

class ToolBarGen_box < Gtk::Toolbar

	def initialize window
	
		super()
		
    @window = window
    
    set_toolbar_style Gtk::Toolbar::Style::BOTH
    
    taille = 48
		
		exploitation_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/exploitation#{taille}.png" ), t.global.farm )
		parcelles_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/parcelle#{taille}.png" ), t.global.parcels )
		cultures_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/legumes#{taille}.png" ), t.global.farmings )
		articles_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/articles#{taille}.png" ), t.global.goods )
		subventions_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/subventions#{taille}.png" ), t.global.grants )
		tiers_tb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/notebook#{taille}.png" ), t.global.customer )
    configuration_tb = Gtk::MenuItem.new t.toolbargen.configuration
    utilisateurs_tb = Gtk::MenuItem.new t.toolbargen.users
    configtb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/gears#{taille}.png" ), t.toolbargen.configuration )
    abouttb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/help-about#{taille}.png" ), t.toolbargen.about )
    deconnectertb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/deconnecter.png" ), t.toolbargen.logout )
    quittb = Gtk::ToolButton.new( Gtk::Image.new( "./resources/icons/application-exit#{taille}.png" ), t.toolbargen.exit )
		
		menu_config = Gtk::Menu.new
		#menu_config.append assistant_tb	
		menu_config.append configuration_tb	
		menu_config.append utilisateurs_tb	
		menu_config.show_all
	
		exploitation_tb.signal_connect( "clicked" ) {
			window.tableau_bord.refresh
			window.affiche window.tableau_bord
		}
		
		parcelles_tb.signal_connect( "clicked" ) {
			window.liste_parcelles.refresh
			window.affiche window.liste_parcelles
			window.liste_parcelles.focus
		}
		
		articles_tb.signal_connect( "clicked" ) {
			window.liste_articles.refresh
			window.affiche window.liste_articles
			window.liste_articles.focus
		}
		
		subventions_tb.signal_connect( "clicked" ) {
			window.liste_subventions.refresh
			window.affiche window.liste_subventions
			window.liste_subventions.focus
		}
		
		cultures_tb.signal_connect( "clicked" ) {
			window.liste_cultures.refresh
			window.affiche window.liste_cultures
			window.liste_cultures.focus
		}
	
		configtb.signal_connect( "clicked" ) {
			menu_config.popup(nil, nil, 0, 0)
		}
	
		configuration_tb.signal_connect( "activate" ) { 
			window.configuration.refresh
			window.affiche window.configuration
		}
	
		abouttb.signal_connect( "clicked" ) { 
			about = About.new window.version
			about.signal_connect('response') { about.destroy }
		}
		  
	  quittb.signal_connect( "clicked" ) { 
	  	window.quit
	  }
	  
	  utilisateurs_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_users
    }
        
    tiers_tb.signal_connect( "clicked" ) { 
			window.affiche window.liste_tiers
			window.liste_tiers.refresh
			window.liste_tiers.focus
    }
    
    deconnectertb.signal_connect( "clicked" ) { 
	  	window.deconnecter
	  }
        
    tool = [exploitation_tb, parcelles_tb, cultures_tb, Gtk::SeparatorToolItem.new, articles_tb, subventions_tb, tiers_tb, Gtk::SeparatorToolItem.new, configtb, Gtk::SeparatorToolItem.new, abouttb, deconnectertb, quittb]
        
    tool.each_index { |i| 
    	self.insert i, tool[i] 
    	tool[i].sensitive = false unless ( i.eql?( tool.count-1 ) or i.eql?( tool.count-2 ) )
    }
	
	end

end
