# coding: utf-8

module MessageController
	
	def MessageController.erreur window, message
		message window, Gtk::MessageDialog::ERROR, message
	end
	
	def MessageController.attention window, message
		message window, Gtk::MessageDialog::WARNING, message
	end
	
	def MessageController.info window, message
		message window, Gtk::MessageDialog::INFO, message
	end
	
	def MessageController.message window, type, message
		
		messagetype = ""
		case type
			when Gtk::MessageDialog::ERROR
				messagetype = "ERREUR"
			when Gtk::MessageDialog::WARNING
				messagetype = "ATTENTION"
			when Gtk::MessageDialog::INFO
				messagetype = "INFO"
		end
		text "=== #{messagetype} : #{message}"
		
		dialog = Gtk::MessageDialog.new(window, 
                                Gtk::Dialog::DESTROY_WITH_PARENT,
                                type,
                                Gtk::MessageDialog::BUTTONS_OK,
                                message)
		response = dialog.run
		dialog.destroy
		
	end
	
	def MessageController.question_oui_non window, message
	
		dialog = Gtk::MessageDialog.new(window, 
	                              Gtk::Dialog::DESTROY_WITH_PARENT,
	                              Gtk::MessageDialog::QUESTION,
	                              Gtk::MessageDialog::BUTTONS_YES_NO,
	                              message)
		response = dialog.run
		dialog.destroy
		return response==Gtk::Dialog::RESPONSE_YES
		
	end
	
	def MessageController.text message
		puts "#{message}\n"
	end

end		
