# coding: utf-8

class ListeUsers_box < Gtk::VBox

	attr_accessor :ajout
	
	def initialize window
	
		super(false, 3)
		
		set_border_width 10
		
		@window = window
		@user = @window.login.user
		
		add agencement
		
		remplir_users
	
	end
	
	def agencement
		
		vboxaction = Gtk::VBox.new false, 3
		vboxaction.pack_start( boutons, false, false, 3 ) 
		vboxaction.pack_start( tableau_users, true, true, 3 ) 
		frame = Gtk::Frame.new
		frame.add vboxaction
		frame.label = "Liste des utilisateurs"
		
		frame
		
	end
	
	def boutons
	
		align = Gtk::Alignment.new 1, 1, 0, 0
		
		hbox = Gtk::HBox.new false, 2
		
		@changepassword = Gtk::Button.new
		hboxsuppr = Gtk::HBox.new false, 2
		hboxsuppr.add Gtk::Image.new( "./resources/icons/password.png" )
		hboxsuppr.add Gtk::Label.new "Changer le mot de passe"
		@changepassword.add hboxsuppr	
		@changepassword.sensitive = false
		@changepassword.signal_connect ("clicked") {
			change_password ( @view.model.get_value @view.selection.selected, 0 )
		}
		
		@modif = Gtk::Button.new
		hboxsuppr = Gtk::HBox.new false, 2
		hboxsuppr.add Gtk::Image.new( "./resources/icons/saisie.png" )
		hboxsuppr.add Gtk::Label.new "Modifier utilisateur"
		@modif.add hboxsuppr	
		@modif.sensitive = false
		@modif.signal_connect ("clicked") {
			modif_user ( @view.model.get_value @view.selection.selected, 0 )
		}
		
		@ajout = Gtk::Button.new
		@ajout.sensitive = false
		hboxajout = Gtk::HBox.new false, 2
		hboxajout.add Gtk::Image.new( "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new "Nouvel utilisateur"
		@ajout.add hboxajout		
		@ajout.signal_connect ("clicked") {
			modif_user 0
		}
		
		hbox.pack_start(@changepassword, false, false, 2)
		hbox.pack_start(@modif, false, false, 2)
		hbox.pack_start(@ajout, false, false, 2)
		
		align.add hbox
		
		align
	
	end
	
	def tableau_users
	
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Integer, Integer)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			modif_user ( @view.model.get_value @view.selection.selected, 0 ) if ( @user.admin )
		}
		
		@view.signal_connect ("cursor-changed") {
			@changepassword.sensitive = ( @user.id.eql?( @view.model.get_value( @view.selection.selected, 0 ) ) or @user.admin )
			@modif.sensitive = @user.admin
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0	
		
		renderer_toggle_admin = Gtk::CellRendererToggle.new
		renderer_toggle_active = Gtk::CellRendererToggle.new
		
		col = Gtk::TreeViewColumn.new("Prénom", renderer_left, :text => 1)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nom", renderer_left, :text => 2)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Login", renderer_left, :text => 3)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Admin ?", renderer_toggle_admin, :active => 4)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Actif ?", renderer_toggle_active, :active => 5)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def remplir_users

		users = Utilisateur.order(:id)
		
		@list_store.clear
		
		users.each do |h| 		
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.firstname
			iter[2] = h.lastname
			iter[3] = h.username
			iter[4] = h.admin ? 1 : 0
			iter[5] = h.active ? 1 : 0
		end
			
	end
	
	def modif_user id
	
		@window.utilisateur.change id
		@window.affiche @window.utilisateur
	
	end
	
	def change_password id
		
		dialog = Gtk::Dialog.new("Mot de pase",
                             @window,
                             Gtk::Dialog::DESTROY_WITH_PARENT,
                             [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_OK ],
                             [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL ]
                             )

		vbox = Gtk::VBox.new false, 2
		hbox1 = Gtk::HBox.new false, 2
		hbox2 = Gtk::HBox.new false, 2
		vbox.add hbox1
		vbox.add hbox2
		hbox1.pack_start( Gtk::Label.new( "Nouveau mot de passe :" ), false, false, 3 )
		motpasse1 = Gtk::Entry.new
		motpasse1.visibility = false
		hbox1.pack_start( motpasse1, false, false, 3 )
		hbox2.pack_start( Gtk::Label.new( "Confirmer mot de passe :" ), false, false, 3 )
		motpasse2 = Gtk::Entry.new
		motpasse2.visibility = false
		hbox2.pack_start( motpasse2, false, false, 3 )
		
		dialog.vbox.add(vbox)
		vbox.show_all
		dialog.run { |response| 
			if response==Gtk::Dialog::RESPONSE_OK then
				if (motpasse1.text==motpasse2.text and !motpasse1.text.empty?) then
					password = BCrypt::Password.create(motpasse1.text)
				
					user = Utilisateur.find(id)
					user.password = password
					user.save
					
					dialog2 = Gtk::MessageDialog.new(dialog, 
			                        Gtk::Dialog::DESTROY_WITH_PARENT,
			                        Gtk::MessageDialog::INFO,
			                        Gtk::MessageDialog::BUTTONS_OK,
			                        "Le mot de passe a été changé avec succés")
					dialog2.run
					dialog2.destroy
				else
					erreur = ""
					erreur += "Les mots de passe ne correspondent pas !\n" unless motpasse1.text==motpasse2.text
					erreur += "Le mot de passe ne peut pas être vide !\n" if motpasse1.text.empty?
					erreur += "Le mot de passe n'a pas été changé."
					dialog2 = Gtk::MessageDialog.new(dialog, 
			                        Gtk::Dialog::DESTROY_WITH_PARENT,
			                        Gtk::MessageDialog::ERROR,
			                        Gtk::MessageDialog::BUTTONS_OK,
			                        erreur)
					dialog2.run
					dialog2.destroy
				end
			end
			dialog.destroy
		}	
	
	end
	
end
