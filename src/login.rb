# coding: utf-8

class Login_box < Gtk::Table
	
	attr_reader :user
	
	def initialize window
	
		super( 3, 3, false )
		
		@window = window
		@user = User.new
		conf = window.config_db.conf
		
		@salt = "A key up to 56 bytes long"
		@password = "password"
		
		i=0
		self.attach( Gtk::Image.new( "./resources/icons/agritux128.png" ), 0, 3, 0, 1 )
		
		i+=1
		align_base = Gtk::Alignment.new 1, 1, 0, 0
		align_base.add Gtk::Label.new( t.login.base )
		self.attach( align_base, 0, 1, i, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		@base = Gtk::ComboBox.new Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String, String, String, Integer)
		renderer_left = Gtk::CellRendererText.new
		@base.pack_start renderer_left, true
		@base.add_attribute renderer_left, "text", 7
		@base.signal_connect("changed") { changement_base @base.active_iter }
		self.attach( @base, 1, 2, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		align0 = Gtk::Alignment.new 1, 1, 0, 0
		align0.add Gtk::Label.new( t.login.user )
		self.attach( align0, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		@utilisateur = Gtk::Entry.new
		self.attach( @utilisateur, 1, 2, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		align3 = Gtk::Alignment.new 1, 0, 0, 0
		align3.add Gtk::Label.new( t.login.password )
		self.attach( align3, 0, 1, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		@mdp = Gtk::Entry.new
		@mdp.visibility = false
		self.attach( @mdp, 1, 2, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		gestion_connexion = Gtk::Button.new
		gestion_connexion.image = Gtk::Image.new( "./resources/icons/add.png" )
		align_gestion_connexion = Gtk::Alignment.new 0, 0.5, 0, 0
		align_gestion_connexion.add gestion_connexion
		self.attach( align_gestion_connexion, 2, 3, 1, 2, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		validebt = Gtk::Button.new
		validebt.image = Gtk::Image.new( "./resources/icons/validate.png" )
		align2 = Gtk::Alignment.new 0, 0.5, 0, 0
		align2.add validebt
		self.attach( align2, 2, 3, 2, 4, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		i+=1
		@remember = Gtk::CheckButton.new t.login.remember
		self.attach( @remember, 1, 3, i, i+1, Gtk::FILL, Gtk::FILL, 5, 5 )
		
		gestion_connexion.signal_connect( "clicked" ) {
			dial_gestion_db = DialGestionDb.new @window
			dial_gestion_db.run { |response| 
				dial_gestion_db.destroy 			
			}
		}
		
		validebt.signal_connect( "clicked" ) {
			verif @utilisateur.text, @mdp.text
		}
		
		@utilisateur.signal_connect( "activate" ) {
			verif @utilisateur.text, @mdp.text
		}
		
		@mdp.signal_connect( "activate" ) {
			verif @utilisateur.text, @mdp.text
		}
			
	end
	
	def login_automatique
		if @remember.active?
			verif @utilisateur.text, @mdp.text
		end 
	end
	
	def verif util, mdp
	
		if (!util.empty? and @base.active_iter) then
				
			id = @base.model.get_value(@base.active_iter,0)
			lancement = @window.config_db.connexion id
			
			if lancement 
				user = Utilisateur.where(:username => util, :active => true).first
				
				password = ""
			
				if user then
				
					@user.id = user.id
					@user.firstname = user.firstname
					@user.lastname = user.lastname
					@user.username = user.username
					@user.admin = user.admin
					password = user.password
					if ( BCrypt::Password.new(password) == mdp ) then 
						store_default_user( util, mdp )
						log
					else
						log_fail
					end
				else
					log_fail
				end
			
			end
		
		else
			if !@base.active_iter
				@window.message_attention t.login.no_database
			end
		end
	
	end
	
	def log_fail
		@window.message_attention t.login.connexion_error
	
	end
	
	def log
	
		@utilisateur.text = ""
		@mdp.text = ""
		@window.demarre_appli
		@window.maximize
		@window.liste_users.ajout.sensitive = @user.admin
	
	end
	
	def store_default_user util, mdp
	
		@window.config_db.conf["default_user"] = @remember.active? ? util : ""
		mdp_encrypt = EzCrypto::Key.encrypt_with_password @password, @salt, mdp
		@window.config_db.conf["default_user_password"] = @remember.active? ? mdp_encrypt : ""
		@window.config_db.conf["auto_login"] = @remember.active?
		@window.config_db.save_config
	
	end
	
	def refresh
		
		remplir_base
		focus
		#login_automatique
		
	end
	
	def remplir_base
		
		@cb = false
		@base.model.clear
		
		bases = @window.config_db.bases

		if bases
			iter_active = nil

			i=0
			bases.each do |b|
				iter = @base.model.append nil
				iter_active = iter if i==0
				iter_active = iter if b["auto_login"]
				iter[0] = i
				iter[1] = b["adapter"]
				iter[2] = b["host"]
				iter[3] = b["database"]
				iter[4] = b["port"]
				iter[5] = b["username"]
				iter[6] = b["password"]
				iter[7] = (b["nom"].to_s.empty? ? b["database"] : b["nom"])
				iter[8] = b["default_user"]
				if b["default_user_password"]
					if b["default_user_password"].empty?
						iter[9] = ""
					else
						iter[9] = (EzCrypto::Key.decrypt_with_password @password, @salt, b["default_user_password"])
					end
				else
					iter[9] = ""
				end
				iter[10] = (b["auto_login"] ? 1 : 0)
				i += 1
			end
		
			@base.active_iter = iter_active
		end
		@cb = true
		changement_base @base.active_iter
		
	end
	
	def changement_base iter
		
		if @cb and iter
			@utilisateur.text = iter[8]
			@mdp.text = iter[9]
			@remember.active = iter[10]==1
		end
			
	end
	
	def focus
		@utilisateur.grab_focus
	end
	
end

class User

	attr_accessor :id, :firstname, :lastname, :username, :admin

end
