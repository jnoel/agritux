# coding: utf-8

class CreateParcelles < ActiveRecord::Migration
  def self.up
    
    create_table :parcelles do |t|
      t.string :code, :null => false
      t.string :designation, :null => false
      t.integer :taille
      t.text :note
      t.timestamps
    end
    
    Parcelle.create :code => "PARCELLE1", :designation => "Parcelle par défaut", :taille => 10000
    #Parcelle.create :code => "PARCELLE2", :designation => "Parcelle de TEST", :taille => 2500
    #Parcelle.create :code => "PARCELLE3", :designation => "Parcelle B2", :taille => 12500
    
  end

  def self.down
    drop_table :parcelles
  end
end

