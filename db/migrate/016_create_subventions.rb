# coding: utf-8

class CreateSubventions < ActiveRecord::Migration
  def self.up
    
    create_table :subventions do |t|
      t.string :designation, :null => false
      t.date :date, :null => false
      t.string :raison
      t.float :montant, :default => 0.0
      t.timestamps
    end
    
  end

  def self.down
    drop_table :subventions
  end
end

