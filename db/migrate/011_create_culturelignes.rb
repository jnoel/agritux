# coding: utf-8

class CreateCulturelignes < ActiveRecord::Migration
  
  def self.up
    create_table :culturelignes do |t|
      t.integer :cycle, :null => false
      t.date :date, :null => false
      t.integer :culture_id, :null => false
      t.integer :article_id, :null => false
      t.string :article_designation, :null => false
      t.integer :parcelle_id, :null => false
      t.integer :tiers_id, :default => 2
			t.string :unite
      t.float :quantite
      t.float :prix_unitaire
      t.float :prix_total
      t.text :note
      t.text :raison
      t.text :methode
      t.timestamps
    end  
  end

  def self.down
    drop_table :culturelignes
  end
  
end

