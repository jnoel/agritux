# coding: utf-8

class ChangeCultures < ActiveRecord::Migration
  
  def self.up    
    change_table :cultures do |t|
      t.float :prix, :default => 0.0
      t.float :subventions, :default => 0.0
      t.string :unite, :defautl => "unité"
    end    
  end

  def self.down
    change_table :cultures do |t|
	 		t.remove :prix
	 		t.remove :subventions
	 		t.remove :unite
		end
  end
  
end

