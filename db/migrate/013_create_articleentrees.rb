# coding: utf-8

class CreateArticleentrees < ActiveRecord::Migration
  
  def self.up
    create_table :articleentrees do |t|
      t.date :date, :null => false
      t.integer :article_id, :null => false
			t.string :unite
      t.float :quantite
      t.float :prix_unitaire
      t.float :prix_total
      t.text :raison
      t.timestamps
    end  
  end

  def self.down
    drop_table :articleentrees
  end
  
end

