# coding: utf-8

class CreateTiers < ActiveRecord::Migration
  def self.up
    
    create_table :tiers do |t|
      t.string :nom, :null => false
      t.integer :typetiers_id, :null => false
      t.text :adresse
      t.string :cp
      t.string :ville
      t.string :tel
      t.string :fax
      t.string :portable
      t.string :mail
      t.string :site
      t.text :note
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    Tiers.create :nom => "Client divers", :typetiers_id => 2
    Tiers.create :nom => "Fournisseur divers", :typetiers_id => 3
    
  end

  def self.down
    drop_table :tiers
  end
end

