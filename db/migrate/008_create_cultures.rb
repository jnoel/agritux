# coding: utf-8

class CreateCultures < ActiveRecord::Migration
  def self.up
    
    create_table :cultures do |t|
      t.string :code, :null => false
      t.string :designation, :null => false
      t.text :description
      t.integer :culturegroupe_id
      t.text :note
      t.timestamps
    end
    
    #Culture.create :code => "CHOUX1", :designation => "Choux Pommes", :culturegroupe_id => 4
    #Culture.create :code => "CAROTTE1", :designation => "Carotte Nantaises", :culturegroupe_id => 5
    #Culture.create :code => "TOMATE1", :designation => "Tomate marmande", :culturegroupe_id => 3
    #Culture.create :code => "FRAISE1", :designation => "Fraise \"capella\"", :culturegroupe_id => 2
    
  end

  def self.down
    drop_table :cultures
  end
end

