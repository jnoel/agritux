# coding: utf-8

class CreateArticlesorties < ActiveRecord::Migration
  
  def self.up
    create_table :articlesorties do |t|
      t.date :date, :null => false
      t.integer :article_id, :null => false
			t.string :unite
      t.float :quantite
      t.text :raison
      t.timestamps
    end  
  end

  def self.down
    drop_table :articlesorties
  end
  
end

