# coding: utf-8

class CreateArticles < ActiveRecord::Migration
  def self.up
    
    create_table :articles do |t|
      t.string :code, :null => false
      t.string :designation, :null => false
      t.text :description
      t.integer :articlegroupe_id
      t.text :note
      t.string :unite
      t.float :prix
      t.timestamps
    end
    
    Article.create :code => "POTASSE", :designation => "Sulfate de potasse", :articlegroupe_id => 1, :unite => "kg", :prix => 0.5
    Article.create :code => "FUMIER", :designation => "Fumier", :articlegroupe_id => 2, :unite => "t", :prix => 45.0
    Article.create :code => "PERPSOL", :designation => "Préparation du sol", :articlegroupe_id => 7, :unite => "heures", :prix => 45.0
    Article.create :code => "PLANTATION", :designation => "Plantation", :articlegroupe_id => 8, :unite => "jours", :prix => 56.0
    Article.create :code => "RECOLTE", :designation => "Récolte", :articlegroupe_id => 8, :unite => "jours", :prix => 56.55
    Article.create :code => "EAURESEAU", :designation => "Eau du réseau", :articlegroupe_id => 6, :unite => "m3", :prix => 0.6
    Article.create :code => "INSEC", :designation => "Insecticide", :articlegroupe_id => 4, :unite => "L", :prix => 0.8
    
  end

  def self.down
    drop_table :articles
  end
end

