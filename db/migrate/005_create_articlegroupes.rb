# coding: utf-8

class CreateArticlegroupes < ActiveRecord::Migration
  def self.up
    
    create_table :articlegroupes do |t|
      t.string :designation, :null => false
      t.integer :parent_id, :default => 0
      t.string :couleur
      t.timestamps
    end
    
    Articlegroupe.create :designation => "Engrais", :parent_id => 0
    Articlegroupe.create :designation => "Matières organiques", :parent_id => 0
    Articlegroupe.create :designation => "Semences/Plants", :parent_id => 0
    Articlegroupe.create :designation => "Produits Phytos", :parent_id => 0
    Articlegroupe.create :designation => "Autres intrants", :parent_id => 0
    Articlegroupe.create :designation => "Irrigation", :parent_id => 0
    Articlegroupe.create :designation => "Mecanisation", :parent_id => 0
    Articlegroupe.create :designation => "Main d'oeuvre", :parent_id => 0
    
  end

  def self.down
    drop_table :articlegroupes
  end
end

