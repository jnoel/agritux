# coding: utf-8

class CreateSocietes < ActiveRecord::Migration
  def self.up
   
    create_table :societes do |t|
      t.string :nom, :null => false
      t.text :adresse
      t.integer :capital
      t.string :tel
      t.string :fax
      t.string :mail
      t.string :site
      t.string :siret
      t.string :rcs
      t.string :naf
      t.string :status
      t.string :logo
      t.timestamps
    end
    
    Societe.create :nom => "Société Test"
    
  end

  def self.down
    drop_table :societes
  end
end

