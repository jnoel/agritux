# coding: utf-8

class ChangeArticles < ActiveRecord::Migration
  
  def self.up    
    change_table :articles do |t|
      t.float :stock, :default => 0.0
    end    
  end

  def self.down
    change_table :articles do |t|
	 		t.remove :stock
		end
  end
  
end

