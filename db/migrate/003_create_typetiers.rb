# coding: utf-8

class CreateTypetiers < ActiveRecord::Migration
  def self.up
    
    create_table :typetiers do |t|
      t.string :designation, :null => false
      t.timestamps
    end
    
    Typetiers.create :designation => "Aucun"
    Typetiers.create :designation => "Client"
    Typetiers.create :designation => "Fournisseur"
    Typetiers.create :designation => "Client et Fournisseur"
    
  end

  def self.down
    drop_table :typetiers
  end
end

