# coding: utf-8

class CreateGed < ActiveRecord::Migration
  def self.up
    
    create_table :gedtiers do |t| 
			t.integer :tiers_id
			t.string :fichier
			t.text :note
      t.timestamps
    end
    
    create_table :gedcultures do |t| 
			t.integer :culture_id
			t.string :fichier
			t.text :note
      t.timestamps
    end
    
    create_table :gedparcelles do |t| 
			t.integer :parcelle_id
			t.string :fichier
			t.text :note
      t.timestamps
    end
    
    create_table :gedarticles do |t| 
			t.integer :article_id
			t.string :fichier
			t.text :note
      t.timestamps
    end
    
  end

  def self.down
	 drop_table :gedtiers
	 drop_table :gedcultures
	 drop_table :gedparcelles
	 drop_table :gedarticles
  end
end

