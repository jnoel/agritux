# coding: utf-8

class CreateCulturegroupes < ActiveRecord::Migration
  def self.up
    
    create_table :culturegroupes do |t|
      t.string :designation, :null => false
      t.integer :parent_id, :default => 0
      t.string :couleur
      t.timestamps
    end
    
    Culturegroupe.create :designation => "Légumes", :parent_id => 0
    Culturegroupe.create :designation => "Fruits", :parent_id => 0
    Culturegroupe.create :designation => "Légumes fruits", :parent_id => 1
    Culturegroupe.create :designation => "Légumes feuilles", :parent_id => 1
    Culturegroupe.create :designation => "Légumes racines", :parent_id => 1
    Culturegroupe.create :designation => "Légumes fleurs", :parent_id => 1
    
  end

  def self.down
    drop_table :culturegroupes
  end
end

