# coding: utf-8

class CreateUtilisateurs < ActiveRecord::Migration
  def self.up
    
    create_table :utilisateurs do |t|
      t.string :firstname, :null => false
      t.string :lastname, :null => false
      t.string :username, :null => false
      t.string :password
      t.boolean :active, :default => true
      t.boolean :admin, :default => false
      t.timestamps
    end
    
    Utilisateur.create :firstname => "Admin", :lastname => "Admin", :username => "admin", :password => "$2a$10$zox0yI/2eDAeKyJdwrdVx.adG2uWqJEqKjZN9UCPhbZc4XiVzIkgq", :admin => true
    
  end

  def self.down
    drop_table :utilisateurs
  end
end

