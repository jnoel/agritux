# coding: utf-8

class Tiers < ActiveRecord::Base
	belongs_to :typetiers
	has_many :culturelignes
	has_many :gedtiers
end

class Typetiers < ActiveRecord::Base
	has_many :tiers
end

class Parcelle < ActiveRecord::Base
	has_many :culturelignes
end

class Culture < ActiveRecord::Base
	belongs_to :culturegroupe
	has_many :culturelignes
	has_many :gedcultures
end

class Cultureligne < ActiveRecord::Base
	belongs_to :culture
	belongs_to :parcelle
	belongs_to :tiers
	belongs_to :article
end

class Culturegroupe < ActiveRecord::Base
	has_many :cultures
end

class Article < ActiveRecord::Base
	belongs_to :articlegroupe
	has_many :articleentrees
	has_many :articlesorties
	has_many :culturelignes
end

class Articlegroupe < ActiveRecord::Base
	has_many :articles
end

class Utilisateur < ActiveRecord::Base
	has_many :documents
end

class Societe < ActiveRecord::Base

end

class Gedtiers < ActiveRecord::Base
	belongs_to :tiers
end

class Gedculture < ActiveRecord::Base
	belongs_to :culture
end

class Gedarticle < ActiveRecord::Base
	belongs_to :article
end

class Schema_migration < ActiveRecord::Base
	
end

class Articleentree < ActiveRecord::Base
	belongs_to :article
end

class Articlesortie < ActiveRecord::Base
	belongs_to :article
end

class Subvention < ActiveRecord::Base

end
