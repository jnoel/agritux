#!/usr/bin/env ruby
# coding: utf-8

f = File.new('agritux.sh', 'w')
f.write("#!/bin/bash\n")
f.write("source ~/.rvm/scripts/rvm\n")
f.write("cd #{File.join(Dir.getwd,'src')}\n")
f.write("ruby agritux.rbw\n")
f.chmod(0770)
f.close

f = File.new(File.join(Dir.home,'.local/share/applications/agritux.desktop'), 'w')
f.write("#!/usr/bin/env xdg-open\n\n")
f.write("[Desktop Entry]\n")
f.write("Encoding=UTF-8\n")
f.write("Name=AgriTux\n")
f.write("Name[fr]=AgriTux\n")
f.write("Name[fr_FR]=AgriTux\n")
f.write("Exec=#{File.join(Dir.getwd,'agritux.sh')}\n")
f.write("Icon=#{File.join(Dir.getwd,'src/resources/icons/agritux128.png')}\n")
f.write("Terminal=false\n")
f.write("Type=Application\n")
f.write("StartupNotify=true\n")
f.write("Categories=Office\n")
f.chmod(0770)
f.close
